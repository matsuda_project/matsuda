<?php include('topo-login.php'); ?>
	<div class="row col-lg-12 col-md-12 col-xs-12 pdm-inicial">
		<div class="container center">
			<div class="row pdm-inicial--container">
				<div class="row pdm-programa--container text-center">
					<div class="lotes lotes-cadastrados">
						<?php include('pdm-section-escolha.php'); ?>
					</div>
				</div>
				<?php include('fazenda-selecionada.php'); ?>
				<div class="col-lg-7 center oswald text-center pdm-escolha--info">
					<div class="row pdm-escolha--text">
						<p>Gado de Corte, a pasto, com 50 animais, início aos 7 meses com média de peso/animal de 105 Kg</p>
					</div>
					<div class="row pdm-escolha--begin">
						<p>Início no programa em 19/08/2016</p>
					</div>
				</div>
				<div class="col-lg-7 col-xs-12 center oswald text-center pdm-escolha--produto">
					<div class="row pdm-escolha--container">
						<div class="block-title">
							<p>Primeiro Produto</p>
						</div>
						<div class="block-container">
							<div class="col-lg-6 col-xs-12 ">
								<div class="col-lg-10 center">
									<div class="block-container--troca">
										<div class="row troca-text">
											<p>Quer trocar de produto? <span>Digite o nome do Novo Produto</span></p>
										</div>
										<div class="row troca-input">
											<input type="text" name="" placeholder="Digitar nome de outro produto">
										</div>
										<div class="row button-troca">
											<button>Trocar</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-xs-12">
								<div class="col-lg-10 center">
									<div class="block-container--produto">
										<div class="row produto-img center">
											<img src="./images/produtos/produto2.png">
										</div>
										<div class="row produto-text text-center">
											<div class="col-lg-8 center ">
												<p>Suplemento Mineral Proteico, Energetico, pronto para uso, em sistema de creep-feeding</p>	
											</div>
										</div>
										<div class="row produto-button">
											<button>ACEITAR PRODUTO SUGERIDO</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>










<?php include('rodape-login.php'); ?>