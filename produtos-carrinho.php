<?php include('topo-pages.php'); ?>
	<?php include('options.php'); ?>
	<?php include('orcamento-menu.php'); ?>
	<div  role="tabpanel" class="tab-pane fade in row col-lg-12 col-xs-12 col-sm-12 produtos-list active" id="lista">
		<div class="container center">
			<div class="row list">
				<div class="carrinho-list--title text-center">
					<p>Informe a quantidade de cada produto</p>
				</div>
				<div class="col-lg-8 center">
					<div class="carrinho-list--container col-lg-12">
						<div class="list-img left col-lg-1">
							<img src="./images/produtos/produto2.png">
						</div>
						<div class="row list-container">
							<div class="left list-block col-lg-5">
								<div class="container">
									<div class="list-title">
										<h1>Matsuda Top Line Cria </h1>
									</div>
									<div class="list-kg">
										<p>25 kg</p>
									</div>
								</div>
							</div>
							<div class="list-qtd left col-lg-2 col-xs-6 text-center container">
								<p>Quantidade de sacos</p>
							</div>
							<div class="list-qtd--count col-lg-1 col-xs-3">
								<div class="input-group spinner center">
								    <input type="text" class="form-control" value="0">
							      	<button class="btn btn-top" type="button"><i class="fa fa-caret-up"></i></button>
							      	<button class="btn btn-bottom" type="button"><i class="fa fa-caret-down"></i></button>
							    </div>
							</div>
							<div class="list-qtd--delete col-lg-2 col-xs-3 left text-center">
								<button>
									<div class="icon">
										<i class="fa fa-times"></i>
									</div>
									<div class="text">
										<p>Excluir</p>
									</div>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row list">
				<div class="col-lg-8 center">
					<div class="carrinho-list--container col-lg-12">
						<div class="list-img left col-lg-1">
							<img src="./images/produtos/produto2.png">
						</div>
						<div class="row list-container">
							<div class="left list-block col-lg-5">
								<div class="container">
									<div class="list-title">
										<h1>Matsuda Top Line Cria </h1>
									</div>
									<div class="list-kg">
										<p>25 kg</p>
									</div>
								</div>
							</div>
							<div class="list-qtd left col-lg-2 col-xs-6 text-center container">
								<p>Quantidade de sacos</p>
							</div>
							<div class="list-qtd--count col-lg-1 col-xs-3">
								<div class="input-group spinner center">
								    <input type="text" class="form-control" value="0">
							      	<button class="btn btn-top" type="button"><i class="fa fa-caret-up"></i></button>
							      	<button class="btn btn-bottom" type="button"><i class="fa fa-caret-down"></i></button>
							    </div>
							</div>
							<div class="list-qtd--delete col-lg-2 col-xs-3 left text-center">
								<button>
									<div class="icon">
										<i class="fa fa-times"></i>
									</div>
									<div class="text">
										<p>Excluir</p>
									</div>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row list">
				<div class="col-lg-8 center">
					<div class="carrinho-list--container col-lg-12">
						<div class="list-img left col-lg-1">
							<img src="./images/produtos/produto2.png">
						</div>
						<div class="row list-container">
							<div class="left list-block col-lg-5">
								<div class="container">
									<div class="list-title">
										<h1>Matsuda Top Line Cria </h1>
									</div>
									<div class="list-kg">
										<p>25 kg</p>
									</div>
								</div>
							</div>
							<div class="list-qtd left col-lg-2 col-xs-6 text-center container">
								<p>Quantidade de sacos</p>
							</div>
							<div class="list-qtd--count col-lg-1 col-xs-3">
								<div class="input-group spinner center">
								    <input type="text" class="form-control" value="0">
							      	<button class="btn btn-top" type="button"><i class="fa fa-caret-up"></i></button>
							      	<button class="btn btn-bottom" type="button"><i class="fa fa-caret-down"></i></button>
							    </div>
							</div>
							<div class="list-qtd--delete col-lg-2 col-xs-3 left text-center">
								<button>
									<div class="icon">
										<i class="fa fa-times"></i>
									</div>
									<div class="text">
										<p>Excluir</p>
									</div>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row carrinho-options--buttons">
				<div class="col-lg-6 center">
					<div class="button-inserir oswald left">
						<button>SELECIONAR OUTROS PRODUTOS</button>
					</div>
					<div class="button-avancar  oswald left">
						<button>AVANÇAR</button>
						<img src="./images/cadastro-buttom.png">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div  role="tabpanel" class="tab-pane fade in row col-lg-12 col-xs-12 informacoes-list" id="informacoes">
		<div class="container center">	
			<div class="row col-lg-6 col-xs-12 form-left">
				<div class="col-lg-7 center">
					<div class="row form-left--title text-center">
						<p>Faça seu login e senha e tenha acesso à nossa área do cliente. É rápido e gratuito</p>
					</div>
					<form>
						<div class="row form-left--container">
							<div class="col-lg-2 col-xs-2 left">
								<p>Nome</p>
							</div>
							<div class="col-lg-10 col-xs-10 left">
								<input type="text" name="">
							</div>
						</div>
						<div class="row form-left--container">
							<div class="col-lg-2 col-xs-2 left">
								<p>E-mail</p>
							</div>
							<div class="col-lg-10 col-xs-10 left">
								<input type="text" name="">
							</div>
						</div>
						<div class="col-lg-12 col-xs-12">
							<div class="form-left--button oswald text-center">
								<button>CADASTRAR</button>
								<img src="./images/cadastro-buttom-up.png">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row col-lg-6 col-xs-12 form-right">
				<div class="col-lg-7 center">
					<div class="row form-left--title text-center">
						<p>Se você já é cadastrado, entre em sua área do cliente e confirme seus dados</p>
					</div>
					<form>
						<div class="row form-right--container">
							<div class="col-lg-2 col-xs-2 left">
								<p>Nome</p>
							</div>
							<div class="col-lg-10 col-xs-10 left">
								<input type="text" name="">
							</div>
						</div>
						<div class="row form-right--container">
							<div class="col-lg-2 col-xs-2 left">
								<p>E-mail</p>
							</div>
							<div class="col-lg-10 col-xs-10 left">
								<input type="text" name="">
							</div>
						</div>
						<div class="col-lg-12 col-xs-12">
							<div class="form-right--button oswald text-center">
								<button>AVANÇAR</button>
								<img src="./images/cadastro-buttom.png">
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- ESTE BLOCO ESTÁ COM DISPLAY NONE -->
			<div class="informacoes-cadastro">
				<div class="cadastro-pf--block">
					<div class="col-lg-6 col-xs-12 left block-control container">
						<div class="block-title">
							<p>Dados pessoais</p>
						</div>
						<div class="block-form block-dados-pessoais">
							<form>
								<div class="form-field">
									<div class="row user-identity">
										<div class="left col-lg-4 col-xs-6">
											<div class="left">
												<input type="radio" aria-label="" name="tipo-user" value="pf" class="radio">
											</div>
											<div class="left">
												<p>Pessoa Física</p>
											</div>
										</div>
										<div class="left col-lg-4 col-xs-6">
											<div class="left">
												<input type="radio" aria-label="" name="tipo-user" value="pj" class="radio">
											</div>
											<div class="left">
												<p>Pessoa Jurídica</p>
											</div>
										</div>
									</div>
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">CPF:</label>
									    </div>
									</div>
									<div class="row user-identity">
										<div class="left col-lg-4 col-xs-6">
											<div class="left">
												<input type="radio" aria-label="" name="sexo" value="m" class="radio">
											</div>
											<div class="left">
												<p>Masculino</p>
											</div>
										</div>
										<div class="left col-lg-4 col-xs-6">
											<div class="left">
												<input type="radio" aria-label="Pessoa Fisica" name="sexo" value="f" class="radio">
											</div>
											<div class="left">
												<p>Feminino</p>
											</div>
										</div>
									</div>
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Nome Completo:</label>
									    </div>
									</div>
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="row">
											<label>Data de Nascimento:</label>
										</div>
										<div class="row">
											<div class="left">
												<select>
								                  	<option>Dia</option>
								                </select>
											</div>
							                <div class="left">
												<select>
								                  	<option>Mês</option>
								                </select>
											</div>
							                <div class="left">
												<select>
								                  	<option>Ano</option>
								                </select>
											</div>
										</div>
									</div>
									<div class="password">
										<div class="row col-lg-12 col-xs-12 input-group">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											    <input class="mdl-textfield__input"  type="text" id="sample3" required>
											    <label class="mdl-textfield__label" for="sample3">Criar Senha:</label>
										    </div>
										</div>
										<div class="row col-lg-12 col-xs-12 input-group">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											    <input class="mdl-textfield__input"  type="text" id="sample3" required>
											    <label class="mdl-textfield__label" for="sample3">Confirmar Senha:</label>
										    </div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-6 col-xs-12 left block-control container">
						<div class="block-title">
							<p>Dados de Contato</p>
						</div>
						<div class="block-form block-dados-contato">
							<form>
								<div class="form-field">
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="email" id="sample3"required>
										    <label class="mdl-textfield__label" for="sample3">E-mail:</label>
									    </div>
									    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Confirmar e-mail:</label>
									    </div>
									    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Telefone Residencial:</label>
									    </div>
									    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Telefone Celular:</label>
									    </div>
									    <div class="col-lg-9 col-xs-9 left">
									    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
										    	<label class="mdl-textfield__label" for="sample3">Rua / Av:</label>
									    	</div>
									    </div>
									    <div class="col-lg-3 col-xs-3 left control container">
									    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
										    	<label class="mdl-textfield__label" for="sample3">Número:</label>
									    	</div>
									    </div>
									    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Complemento:</label>
								    	</div>
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Bairro:</label>
								    	</div>
								    	<div class="col-lg-9 col-xs-9 left">
									    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
										    	<label class="mdl-textfield__label" for="sample3">Cidade:</label>
									    	</div>
								    	</div>
								    	<div class="col-lg-3 col-xs-3 control container">
									    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    	<div class="left col-lg-12">
													<select>
									                  	<option>Estado</option>
									                </select>
												</div>
									    	</div>
								    	</div>
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">País:</label>
								    	</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-6 col-xs-12 left block-control container">
						<div class="block-title">
							<p>Gostariamos de saber</p>
						</div>
						<div class="block-form block-dados-newsletter">
							<form>
								<div class="form-field">
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="col-lg-4 col-xs-12">
											<label>É cliente Matsuda?</label>
										</div>
										<div class="col-lg-4 col-xs-12 input-group--cliente">
											<div class="col-lg-6 col-xs-6">
												<div class="left">
													<input type="radio" name="cliente" value="sim" class="radio">
												</div>
												<div class="left">
													<p>Sim</p>
												</div>
											</div>
											<div class="col-lg-6 col-xs-6">
												<div class="left">
													<input type="radio" name="cliente" value="nao" class="radio">
												</div>
												<div class="left">
													<p>Não</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row col-lg-12 col-xs-12 input-group info">
										<div class="col-lg-12 col-xs-12">
											<label>Quero receber e-mail’s sobre produtos dos segmentos:</label>
										</div>
										<div class="col-lg-6 col-xs-12 left">
											<div class="row col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Nutrição Animal">
												</div>
												<div class="left">
													<p>Nutrição Animal</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Sementes para Pastagem">
												</div>
												<div class="left">
													<p>Sementes para Pastagem</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Equipamentos Agrícolas">
												</div>
												<div class="left">
													<p>Equipamentos Agrícolas</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Inoculantes para Silagem">
												</div>
												<div class="left">
													<p>Inoculantes para Silagem</p>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-xs-12 left">
											<div class="row col-lg-12  col-xs-12 checkbox-control">
												<div class="left"> 
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Saúde Animal">
												</div>
												<div class="left">
													<p>Saúde Animal</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Rações para Peixes">
												</div>
												<div class="left">
													<p>Rações para Peixes</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Energia Solar">
												</div>
												<div class="left">
													<p>Energia Solar</p>
												</div>
											</div>
											<div class="col-lg-12 col-xs-12 checkbox-control">
												<div class="left">
													<input type="checkbox" name="receberemail" id="" class="checkbox" value="Alimentos Pet">
												</div>
												<div class="left">
													<p>Alimentos Pet</p>
												</div>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 newsletter-emails">
											<div class="row col-xs-12 ">
												<label>Aceito receber promoções e e-mails institucionais</label>
											</div>
											<div class="col-lg-2 col-xs-6">
												<div class="left">
													<input type="radio" name="newsletter" value="sim" class="radio">
												</div>
												<div class="left">
													<p>Sim</p>
												</div>
											</div>
											<div class="col-lg-2 col-xs-6">
												<div class="left">
													<input type="radio" name="newsletter" value="nao" class="radio">
												</div>
												<div class="left">
													<p>Não</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12 row">
						<div class="row btn-confirmar">
							<div class="col-lg-7 center">
								<div class="text-center oswald col-lg-6 return">
									<img src="./images/cadastro-buttom-left.png">
									<button type="submit">RETORNAR</button>
								</div>	
								<div class="text-center oswald col-lg-6 avancar">
									<button type="submit">CADASTRAR</button>
									<img src="./images/cadastro-buttom.png">
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div  role="tabpanel" class="tab-pane fade in row col-lg-12 col-xs-12 conformar-list " id="confirmar">
		<div class="container center">
			<div class="col-lg-6 center col-xs-12 confirmar-list--container">
				<div class="text-center">
					<div class="confirmar-title">
						<p>Seus dados estão corretos?</p>
					</div>
					<div class="confirmar-container">
						<div class="container-name">
							<p>Aldinei Souza Franco</p>
						</div>
						<div class="container-email">
							<p>aldineifranco@yahoo.com.br</p>
						</div>
						<div class="container-end">
							<p>Rua Elizeu Vila Real, 406</p>
						</div>
						<div class="container-city">
							<p>Presidente Prudente - SP</p>
						</div>
						<div class="container-phone">
							<p>(18) 99748-6943</p>
						</div>
					</div>
					<div class="confirmar-text">
						<p>Se alguma informação mudou <a href="">atualize</a> seu cadastro</p>
					</div>
					<div class="row confimar-buttons">
						<div class="text-center oswald col-lg-6 return">
							<img src="./images/cadastro-buttom-left.png">
							<button type="submit">RETORNAR</button>
						</div>	
						<div class="text-center oswald col-lg-6 avancar">
							<button type="submit">CADASTRAR</button>
							<img src="./images/cadastro-buttom.png">
						</div>
					</div>
				</div>
			</div>
			<!-- ESTE BLOCO ESTÁ COM DISPLAY NONE -->
			<div class="col-lg-6 center alterar-dados">
				<div class="alterar-dados--title text-center">
					<p>Alterar Dados</p>
				</div>
				<div class="block-form block-dados-contato">
					<form>
						<div class="form-field">
							<div class="row col-lg-12 input-group">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
								    <input class="mdl-textfield__input"  type="email" id="sample3"required>
								    <label class="mdl-textfield__label" for="sample3">nome</label>
							    </div>
							    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
								    <input class="mdl-textfield__input"  type="email" id="sample3"required>
								    <label class="mdl-textfield__label" for="sample3">e-mail</label>
							    </div>
							    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
								    <input class="mdl-textfield__input"  type="text" id="sample3" required>
								    <label class="mdl-textfield__label" for="sample3">confirmar e-mail</label>
							    </div>
							    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
								    <input class="mdl-textfield__input"  type="text" id="sample3" required>
								    <label class="mdl-textfield__label" for="sample3">telefone fixo</label>
							    </div>
							    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
								    <input class="mdl-textfield__input"  type="text" id="sample3" required>
								    <label class="mdl-textfield__label" for="sample3">telefone comercial</label>
							    </div>
							    <div class="col-lg-12 col-xs-12">
							    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-9 col-xs-9">
								    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
								    	<label class="mdl-textfield__label" for="sample3">endereço</label>
							    	</div>
								    <div class="col-lg-3 col-xs-3 left control container">
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Nº.</label>
								    	</div>
								    </div>
							    </div>
							    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
							    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
							    	<label class="mdl-textfield__label" for="sample3">complemento</label>
						    	</div>
						    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
							    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
							    	<label class="mdl-textfield__label" for="sample3">bairro</label>
						    	</div>
						    	<div class="col-lg-12 col-xs-12">
							    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-9 left">
								    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
								    	<label class="mdl-textfield__label" for="sample3">cidade</label>
							    	</div>
							    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-3 left">
								    	<div class="left col-lg-12 select">
											<select>
							                  	<option>Estado</option>
							                </select>
										</div>
							    	</div>
						    	</div>
						    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-lg-12 col-xs-12">
							    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
							    	<label class="mdl-textfield__label" for="sample3">País:</label>
						    	</div>
							</div>
							<div class="row col-lg-12 input-group info">
								<div class="col-lg-10 center">
									<div class="col-lg-12 control-seg">
										<label>Quero receber e-mail’s sobre produtos dos segmentos:</label>
									</div>
									<div class="col-lg-8 left">
										<div class="row col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Nutrição Animal">
											</div>
											<div class="left">
												<p>Nutrição Animal</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Sementes para Pastagem">
											</div>
											<div class="left">
												<p>Sementes para Pastagem</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Equipamentos Agrícolas">
											</div>
											<div class="left">
												<p>Equipamentos Agrícolas</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Inoculantes para Silagem">
											</div>
											<div class="left">
												<p>Inoculantes para Silagem</p>
											</div>
										</div>
									</div>
									<div class="col-lg-4 right">
										<div class="row col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Saúde Animal">
											</div>
											<div class="left">
												<p>Saúde Animal</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Rações para Peixes">
											</div>
											<div class="left">
												<p>Rações para Peixes</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Energia Solar">
											</div>
											<div class="left">
												<p>Energia Solar</p>
											</div>
										</div>
										<div class="col-lg-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Alimentos Pet">
											</div>
											<div class="left">
												<p>Alimentos Pet</p>
											</div>
										</div>
									</div>
									<div class="col-lg-12 newsletter-emails align">
										<div class="row control">
											<label>Aceito receber promoções e e-mails institucionais</label>
										</div>
										<div class="col-lg-2">
											<div class="left">
												<input type="radio" name="newsletter" value="sim" class="radio">
											</div>
											<div class="left">
												<p>Sim</p>
											</div>
										</div>
										<div class="col-lg-2">
											<div class="left">
												<input type="radio" name="newsletter" value="nao" class="radio">
											</div>
											<div class="left">
												<p>Não</p>
											</div>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
	<div  role="tabpanel" class="tab-pane fade in row col-lg-12 col-xs-12 finalizar-list" id="finalizar">
		<div class="container center">	
			<div class="finalizar-list--container text-center">
				<div class="row finalizar-status">
					<p>Pedido <span>0001/16</span> enviado com sucesso!</p>
				</div>
				<div class="row finalizar-status--text">
					<p>Em breve a empresa que representa a Matsuda em sua região entrará em contato</p>
				</div>
				<div class="row finalizar-status--info">
					<p>Lembramos que em sua <a href="">área de cliente</a> encontra-se o <a href="">histórico</a> com todos os seus pedidos de orçamento.</p>
				</div>
				<div class="row finalizar-status--block">
					<img src="./images/cadastroOk.png">
					<div class="block-text">
						<p>Retornar à <a href=""><i>Página Inicial</i></a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="row finalizar-list--produtos">
			<div class="col-lg-12 center">
				<div class="row list-container">
					<div class="col-lg-6 center">
						<div class="row list-container--block">
							<div class="list-container--img col-lg-2 col-xs-3">
								<img src="./images/produtos/produto2.png">	
							</div>
							<div class="list-container--qtd oswald col-lg-6 col-xs-4 text-center">
								<p><i>10 sacos</i></p>
							</div>
							<div class="list-container--block col-lg-4 col-xs-5">
								<div class="block-name">
									<h1>Matsuda Top Line Cria </h1>
								</div>
								<div class="block-resume">
									<h2>Embalagens de 25 kg</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row list-container">
					<div class="col-lg-6 center">
						<div class="row list-container--block">
							<div class="list-container--img col-lg-2 col-xs-3">
								<img src="./images/produtos/produto2.png">	
							</div>
							<div class="list-container--qtd oswald col-lg-6 col-xs-4 text-center">
								<p><i>10 sacos</i></p>
							</div>
							<div class="list-container--block col-lg-4 col-xs-5">
								<div class="block-name">
									<h1>Matsuda Top Line Cria </h1>
								</div>
								<div class="block-resume">
									<h2>Embalagens de 25 kg</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row list-container">
					<div class="col-lg-6 center">
						<div class="row list-container--block">
							<div class="list-container--img col-lg-2 col-xs-3">
								<img src="./images/produtos/produto2.png">	
							</div>
							<div class="list-container--qtd oswald col-lg-6 col-xs-4 text-center">
								<p><i>10 sacos</i></p>
							</div>
							<div class="list-container--block col-lg-4 col-xs-5">
								<div class="block-name">
									<h1>Matsuda Top Line Cria </h1>
								</div>
								<div class="block-resume">
									<h2>Embalagens de 25 kg</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row col-lg-12 col-xs-12 finalizar-list--button">
				<div class="text-center leelawadee">
					<img src="./images/imprimir.png">
					<button>IMPRIMIR</button>
				</div>
			</div>
		</div>
	</div>









<?php include('rodapehome.php'); ?>