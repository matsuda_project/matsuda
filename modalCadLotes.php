<div class="modalCadastroLotes" id="ModalCadLotes">
	<div class="cadastro-block col-lg-12 col-xs-12">
		<div class="cadastro-block--title text-center">
			<p>Cadastrar lote da Fazenda 3 Ilhas</p>
		</div>
		<div class="col-lg-11 col-xs-12 center">
			<div class="row cadastro-container">
				<div class="cadastro-container--form oswald text-center">
					<div class="container">
						<form>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Nome do Lote</label>
								</div>
								<div class="left col-lg-5 col-xs-7">
									<input type="text" name="">
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Espécie Animal</label>
								</div>
								<div class="left col-lg-5 col-xs-7">
									<select>
										<option>Gado de Corte</option>
									</select>
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Sistema</label>
								</div>
								<div class="left col-lg-5 col-xs-7">
									<select>
										<option>Pasto</option>
									</select>
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Nome do Município</label>
								</div>
								<div class="left col-lg-5 col-xs-7">
									<input type="text" name="">
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Quantidade de Animais no Lote</label>
								</div>
								<div class="left col-lg-5 col-xs-7">
									<input type="text" name="">
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Idade</label>
								</div>
								<div class="left col-lg-5 col-xs-5">
									<select>
										<option>7</option>
									</select>
								</div>
								<div class="left col-lg-2 col-xs-1">
									<label><i>em meses</i></label>
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Peso Médio do Lote</label>
								</div>
								<div class="left col-xs-5 col-lg-5">
									<input type="text" name="">
								</div>
								<div class="left col-lg-2">
									<label><i>em Kg</i></label>
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-md-12 control">
								<div class="left col-lg-5 col-xs-5 text-right">
									<label>Data de Início no Programa</label>
								</div>
								<div class="left col-xs-5 col-lg-5">
									<input type="text" name="">
								</div>
								<div class="left col-lg-2 col-xs-2">
									<label><i>DD/MM/AAAA</i></label>
								</div>
							</div>
							<button type="submit">INSERIR</button>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>