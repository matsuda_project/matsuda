<?php include('topo-pages.php'); ?>
<?php include('options.php'); ?>
	
	<div class="row col-lg-12 col-xs-12 descricao-produto">
		<div class="row descricao-container">
			<div class="container center">
				<div class="row col-lg-12 col-xs-12">
					<div class="descricao-container--title">
						<h1>MATSUDA FÓS PRIME</h1>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="descricao-container--img">
						<img src="./images/produto-desc.png">
					</div>
				</div>
				<div class="col-lg-4">
					<div class="descricao-container--ind text-center">
						<div class="ind-block container">
							<div class="col-lg-8 center">
								<div class="ind-block--title">
									<h1>INDICAÇÃO</h1>
								</div>
								<div class="ind-block--content">
									<p>Suplemento Mineral proteico energético, pronto para uso, para rebanhos de cria, recria e engorda a pasto.</p>
								</div>
								
							</div>
						</div>
						<div class="ind-block b2 container">
							<div class="col-lg-10 center">
								<div class="ind-block--title">
									<h1>CONSUMO MÉDIO DIÁRIO RECOMENDADO</h1>
								</div>
								<div class="ind-block--content">
									<p>200 g para cada 100 kg de peso vivo</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row especificacoes-container">
						<div class="row especificacoes-content">
							<div class="col-lg-10 col-xs-12 center ">
								<div class="col-lg-4 col-xs-4 left">
									<div class="esp-block text-center">
										<div class="esp-block--title">
											<h1>EMBALAGEM</h1>
										</div>
										<div class="esp-block--img">
											<img src="./images/embalagem.png">
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-xs-4 left">
									<div class="esp-block text-center ">
										<div class="esp-block--title">
											<h1>PERÍODO</h1>
										</div>
										<div class="esp-block--img">
											<img src="./images/chuvoso.png">
										</div>
										<div class="esp-block--text">
											<p>chuvoso</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-xs-4 left">
									<div class="esp-block text-center ">
										<div class="esp-block--title">
											<h1>Espécie</h1>
										</div>
										<div class="esp-block--img">
											<img src="./images/bovino.png">
										</div>
										<div class="esp-block--text">
											<div class="col-lg-8 center">
												<p>bovinos de corte</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row informacoes-container">
							<div class="col-lg-10 center info-content container">
								<div class="info-block--orc">
									<button type="button" id="BtnModalOrc"><img src="./images/orcamento.png"></button>
								</div>
								<div class="col-lg-8">
									<div class="row info-block--options">
										<div class="options-container oswald">
											<div class="options-container--block left">
												<a href="" id="BtnComparar">
													<img src="./images/icons/troca.png">
													<span>Compare dois produtos</span>
												</a>
											</div>
											<div class="options-container--block left">
												<a href="#" id="BtnFavoritos">
													<img src="./images/icons/like.png">
													<span>Salve em seus favoritos</span>
												</a>
											</div>
											<div class="options-container--block left">
												<a href="" id="BtnLogado">
													<img src="./images/icons/mais.png">
													<span>??</span>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include('descricao-abas-menu.php'); ?>

	<div class="tab-content ">
		<div id="clientes" role="tabpanel" class="tab-pane fade row col-lg-12 col-xs-12 clientes active in">
	        <div class="row emdestaque container center">
                <div class="colVideo Seg_sementes container col-lg-3">
                	<div class="col-lg-9">
	                    <div class="video">
	                        <img src="img/emdestaque.png" class="imgDestaque" />
	                        <div class="hoverTitle text-center col-lg-2 col-xs-12 col-sm-12 col-md-2 center">
	                           <img src="./img/iconRun.png">
	                        </div>
	                        <ul class="likeEmdestaque">
	                         	<li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
	                        	<li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
	                       		<li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
	                      	</ul>
	                      	<div class="categoria">NUTRIÇÃO ANIMAL</div>
	                    </div>
	                    <p class="titulo">Matsuda Fós Prime no semi-confinamento (Fazenda MG)</p>            
                	</div>
                </div>

                <div class="colVideo Seg_sementes container col-lg-3">
                	<div class="col-lg-9">
	                    <div class="video">
	                      <img src="img/emdestaque.png" class="imgDestaque" />
	                      <div class="hoverTitle text-center col-lg-2 col-xs-12 col-sm-12 col-md-2  center">
	                         <img src="./img/iconRun.png">
	                      </div>
	                      <ul class="likeEmdestaque ">
	                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
	                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
	                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
	                      </ul>
	                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
	                    </div>
	                    <p class="titulo">Matsuda Fós Prime (Sítio São Paulo</p>  
                    </div>          
                </div>

                <div class="colVideo Seg_sementes container col-lg-3">
                	<div class="col-lg-9">
	                    <div class="video">
	                      <img src="img/emdestaque.png" class="imgDestaque" />
	                      <div class="hoverTitle text-center col-lg-2 col-xs-12 col-sm-12 col-md-2  center">
	                         <img src="./img/iconRun.png">
	                      </div>
	                      <ul class="likeEmdestaque">
	                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
	                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
	                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
	                      </ul>
	                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
	                    </div>
	                    <p class="titulo">Matsuda Fós Prime (Estância JM Agropecuária</p>  
	                </div>          
                </div>

                <div class="colVideo Seg_sementes container col-lg-3">
                	<div class="col-lg-9">
	                    <div class="video">
	                      <img src="img/emdestaque.png" class="imgDestaque" />
	                      <div class="hoverTitle text-center col-lg-2 col-xs-12 col-sm-12 col-md-2  center">
	                         <img src="./img/iconRun.png">
	                      </div>
	                      <ul class="likeEmdestaque">
	                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
	                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
	                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
	                      </ul>
	                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
	                    </div>
	                    <p class="titulo">Matsuda Fós Prime (Fazenda São José)</p>  
                    </div>          
                </div>
                <div class="row col-lg-12 col-xs-12 videos-plus text-center">
                	<div class="videos-plus--block oswald">
                		<a href="">MAIS VÍDEOS</a>
                	</div>
                </div>        
	        </div>
		</div>
		<div id="garantia" role="tabpanel" class="tab-pane fade row col-lg-12 col-xs-12 garantia in">
			<div class="row col-lg-12 col-xs-12 garantia-container">
				<div class="container center">
					<div class="col-lg-3 left">
						<div class="garantia-table">
							<table>
								<tr>
									<td width="205">Cálcio (máx.)</td>
									<td width="95">30 g</td>
								</tr>
								<tr>
									<td width="205">Cálcio (min.)</td>
									<td width="95">20 g</td>
								</tr>
								<tr>
									<td width="205">Fosfóro (mín.)</td>
									<td width="95">9.000 mg</td>
								</tr>
								<tr>
									<td width="205">Sódio (mín.)</td>
									<td width="95">37 g</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-lg-3 left">
						<div class="garantia-table">
							<table>
								<tr>
									<td width="205">Enxofre (mín.)</td>
									<td width="95">16 g</td>
								</tr>
								<tr>
									<td width="205">Magnésio (mín.)</td>
									<td width="95">2.000 mg</td>
								</tr>
								<tr>
									<td width="205">Cobalto (mín.)</td>
									<td width="95">20 mg</td>
								</tr>
								<tr>
									<td width="205">Cobre (mín)</td>
									<td width="95">150 mg</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-lg-3 left">
						<div class="garantia-table">
							<table>
								<tr>
									<td width="205">Iodo (mín.)</td>
									<td width="95">17 mg</td>
								</tr>
								<tr>
									<td width="205">Manganês (mín.)</td>
									<td width="95">140 mg</td>
								</tr>
								<tr>
									<td width="205">Selênio (mín.)</td>
									<td width="95">3 mg</td>
								</tr>
								<tr>
									<td width="205">Zinco (mín)</td>
									<td width="95">600 mg</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-lg-3 left">
						<div class="garantia-table">
							<table>
								<tr>
									<td width="205">Ferro (mín.)</td>
									<td width="95">100 mg</td>
								</tr>
								<tr>
									<td width="205">Flúor (máx.)</td>
									<td width="95">90 mg</td>
								</tr>
								<tr>
									<td width="205">Proteína Bruta (mín.)</td>
									<td width="95">200 g</td>
								</tr>
								<tr>
									<td width="205">N.D.T (mín)</td>
									<td width="95">700 g</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade row col-lg-12 col-xs-12 modo-usar in" id="usar">
			<div class="container center">
				<div class="modo-usar--text text-center">
					<div class="col-lg-5 center">
						<p>Deve ser fornecido puro e de forma controlada em cochos apropriados com pelo menos 4 metros de comprimento para cada 10 cabeças</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row col-lg-12 col-xs-12 produtos-relacionados" id="relacionados" role="tabpanel">
			<div class="container center">
				<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container control">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-xs-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center ">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container control">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-xs-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center ">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container control">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-xs-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center ">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container control">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-xs-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center ">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			</div>
		</div>
	</div>




<?php include('modalFavoritosLogado.php'); ?>
<?php include('modalComparar.php'); ?>
<?php include('modalOrcamento.php'); ?>
<?php include('modalFavoritosLogin.php'); ?>
<?php include('rodapehome.php'); ?>