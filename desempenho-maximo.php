<?php include('topo-pages.php'); ?>

	<div class="row col-lg-12 col-xs-12 desempenho-maximo">
		<div class="container center">
			<div class="desempenho-block text-center col-lg-12">
				<div class="row">
					<div class="col-lg-6 center">
						<div class="desempenho-block--img col-lg-6">
							<img src="./images/desempenhomaximo.png">
						</div>
						<div class="desempenho-block--content col-lg-6">
							<p>
								<span>Programa</span>
								<span>DESEMPENHO</span>
								<span>MÁXIMO</span>
								<span>MATSUDA</span>
							</p>
						</div>
					</div>
				</div>
				<div class="row desempenho-block--text">
					<div class="col-lg-6 center">
						<p>O Programa Desempenho Máximo, foi elaborado pelo Departamento Técnico de Nutrição Animal do grupo MATSUDA, que tem como 		objetivo principal fazer com que as propriedades rurais atinjam seus melhores Desempenhos, possibilitando com isso 			aumentar suas margens de lucro, uma vez que busca trabalhar com todo o potencial de seus animais. Seja a propriedade 		produtora de bezerros, produtora de carne ou leite, a pasto ou em sistema de confinamento, e até mesmo as propriedades 		de ciclo completo, o programa Desempenho Máximo tem um modelo de trabalho que visa através da correta suplementação 		mineral dos animais, aumentar as taxas de produção. O projeto Desempenho Máximo é hoje direcionado a cinco 				atividades: Produção de bezerros (as) com o Programa <span class="mais">+</span><span class="comp">Bezerros</span>; Produção de carne a pasto ou em sistema de 			confinamento, com os Programas <span class="mais">+</span><span class="comp">Carne</span> <span class="comp2">Pasto</span> e <span class="mais">+</span><span class="comp">Carne</span> <span class="comp3">Confinamento</span>;Produção de leite a pasto, através do Programa <span class="mais">+</span><span class="comp">Leite</span> <span class="comp2">Pasto</span> e também no sistema intensivo em confinamento pelo Programa <span class="mais">+</span><span class="comp">Leite</span> <span class="comp3">Intensivo</span> Em breve, apresentaremos dentro do programa Desempenho Máximo, o programa <span class="mais">+</span><span class="comp">Cordeiros</span>.
							</p>
					</div>
					<div class="col-lg-6 center">
						<div class="desempenho-link oswald">
							<a href="">MONTE O SEU PROGRAMA DESEMPENHO MÁXIMO</a>
						</div>
					</div>
				</div>
				<div class="mobile-hide tablet-hide">
					<div class="row">
						<div class="atividades">
							<div class="atividades-block">
								<div class="atividades-block--title">
									<p>ATIVIDADES</p>
								</div>
								<div class="atividades-block--content">
									<p>selecione e saiba mais</p>
								</div>
								<div class="atividades-link">
									<a href=""><img src="./img/setaBaixo.png"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="programas">
							<div class="col-lg-2 col-xs-12 left">
								<div class="programas-block container">
									<div class="programas-container">
										<div class="programas-block--title control">
											<img src="./images/bezerros.png">
										</div>
										<div class="programas-block--img">
											<img src="./images/desempenhomaximo.png">
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-xs-12  left">
								<div class="programas-block container">
									<div class="programas-container">
										<div class="programas-block--title">
											<img src="./images/carne.png">
										</div>
										<div class="programas-block--img">
											<img src="./images/desempenhomaximo2.png">
										</div>
									</div>							
								</div>
							</div>
							<div class="col-lg-2 col-xs-12  left">
								<div class="programas-block container">
									<div class="programas-container">
										<div class="programas-block--title">
											<img src="./images/confinamento.png">
										</div>
										<div class="programas-block--img">
											<img src="./images/desempenhomaximo2.png">
										</div>
									</div>						
								</div>
							</div>
							<div class="col-lg-2 col-xs-12  left">
								<div class="programas-block container">
									<div class="programas-container">
										<div class="programas-block--title">
											<img src="./images/leite.png">
										</div>
										<div class="programas-block--img">
											<img src="./images/desempenhomaximo3.png">
										</div>
									</div>							
								</div>
							</div>
							<div class="col-lg-2 col-xs-12 left">
								<div class="programas-block container">
									<div class="programas-container">
										<div class="programas-block--title">
											<img src="./images/leiteintensivo.png">
										</div>
										<div class="programas-block--img">
											<img src="./images/desempenhomaximo3.png">
										</div>
									</div>							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	

		</div>
	</div>



<?php include('rodapehome.php'); ?>