<?php include('topo-pages.php'); ?>

<?php include('topo-desempenho.php'); ?>
	<div class="tab-content">
		<div id="entenda" role="tabpanel" class="tab-pane fade row col-lg-12 col-xs-12 desempenho-entenda active in">
			<div class="container center">
				<div class="col-lg-6 center">
					<div class="desempenho-entenda--content text-center">
						<p>No Programa <span class="mais">+</span><span class="comp">Carne</span> <span class="comp2">Confinamento</span>, buscamos a viabilidade do sistema, pois a atividade de confinamento de bovinos de corte em nosso País vem passando a cada dia por constantes aprimoramentos, tanto na gestão técnica quanto na econômica. E assim, como as demais atividades econômicas, está constantemente em busca de novas soluções ou oportunidade para aumentar sua rentabilidade. Em um passado não muito distante, a simples valorização no preço das arrobas do boi gordo entre a safra e entressafra, que chegava a ser de até 30%, já justificava o sistema de confinamento. No entanto, o cenário da valorização das arrobas ao longo do ano e os preços das commodities mudaram, e a modalidade de confinamento se tornou uma atividade de risco, em que apenas a valorização das arrobas estocadas não traz mais a garantia de sucesso ao empreendimento. No entanto, a necessidade do controle de gestão de dados, capacitação profissional, manutenções preventivas em equipamentos e instalações, e é claro, sem esquecer do tão importante manejo nutricional com os animais, passaram a ser rotina constante nas operações de confinamento, afim de se obter a precocidade produtiva na produção destes animais. Neste contexto, os resultados econômicos passaram a estar diretamente ligados ao plano nutricional, que por sua vez representa 70 a 80% dos custos de produção, quando não consideramos o boi magro. E para isso, a utilização de <span class="comp3">NÚCLEOS MINERAIS</span> específicos para animais confinados é extremamente importante, uma vez que estes animais em sua grande maioria são provenientes do sistema de produção a pasto, e muitos ainda desistemas extensivos, os quais podem não estar habituados com a ingestão de grãos e/ou farelos junto a suas dietas, precisando estes serem adaptados à uma nova dieta. Devemos lembrar que no início do confinamento os animais passarão por um período de adaptação, que irá prepará-los para as alterações em seu metabolismo, ocasionada pela nova dieta, além de reduzir o estresse causado pelo novo modelo de criação. Para isso, um núcleo mineral com maiores níveis de macro e microminerais, além de alguns aditivos, são de suma importância para uma boa adaptação metabólica, afim de reduzir distúrbios digestivos, o estresse e a recusa de cocho por parte dos animais confinados.
						Durante todo o período de confinamento é necessário que se tenha garantido o atendimento do requerimento nutricional do animal, sendo que, além do aporte proteico e energético, é preciso lembrar dos requerimentos de minerais, fazendo-se uso de Núcleos Minerais apropriados para a terminação, que forneçam todo o aporte mineral que os animais necessitam, uma vez que estes estão intimamente ligados a síntese de carboidratos, proteína, mobilização da gordura corpórea e manutenção do sistema imunológico do animal. Sendo assim, o Programa <span class="mais">+</span><span class="comp">Carne</span> <span class="comp2">Confinamento</span>, visa realizar o abate de animais com até dois anos de idade, ou seja, com até dois dentes incisivos permanentes, de modo que se tenha um animal gordo e com uma carcaça muito bem acabada e prontos para o abate, mesmo em uma fase crítica de oferta de alimento, buscando a maximização dos lucros e a redução no tempo de retorno do capital. Nesta fase, a presença de um técnico nutricionista para a formulação e acompanhamento da dieta, assim como todo o manejo são determinantes para se alcançar o máximo de desempenho dos animais neste sistema.
						 
						</p>
					</div>
					<div class="row">
						<div class="desempenho-entenda--link text-center oswald">
							<a href="">MONTE O SEU PROGRAMA DESEMPENHO MÁXIMO</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row col-lg-12 col-xs-12 desempenho-grafico" id="grafico" role="tabpanel">
			<div class="container center">
				<div class="desempenho-grafico--container">
					<div class="grafico-block--text left col-lg-3 col-xs-12">
						<div class="grafico-block--border left">
						</div>
						<div class="col-lg-6 col-xs-6 container">
							<div class="col-lg-12 left">
								<p>PASSE O MOUSE
								SOBRE O PRODUTO
								PARA CONHECER
								SUA INDICAÇÃO</p>
							</div>
						</div>
						<div class="col-lg-4 col-xs-5">
							<img src="./images/seta-grafico.png">
						</div>
					</div>
					<div class="col-lg-6 col-xs-12">
						<div class="grafico-block--img">
							<img src="./images/grafico.png">
						</div>
					</div>
					<div class="col-lg-3 col-xs-12 col-md-3 grafico-block--container">
						<div class="row block-title">
							<div class="col-lg-6 center text-center ">
								<h1>Matsuda Top Bezerro</h1>
							</div>
						</div>
						<div class="row block-content">
							<div class="block-content--img col-lg-5">
								<img src="./images/produto-grafico.png">
							</div>
							<div class="block-content--text col-lg-7">
								<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
							</div>
						</div>
						<div class="row block-link">
							<div class="text-center">
								<a href="">Mais Informações</a>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-link--bottom text-center oswald">
							<a href="">MONTE O SEU PROGRAMA DESEMPENHO MÁXIMO</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row col-lg-12 col-xs-12 desempenho-produtos" id="produtos" role="tabpanel">
			<div class="container center">
				<div class="desempenho-produtos--container">
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center ">
										<h1>Matsuda Top Bezerro</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produto-grafico.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Matsuda Top Bezerro Desmama</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto2.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Matsuda Line <br>Recria</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto3.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Winter Fós Gold Seca</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto4.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Winter Fós Gold Seca</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto4.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Winter Fós Gold Seca</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto5.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Winter Fós Gold Seca</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto6.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 left container">
						<div class="produtos-block">
							<div class="produtos-block--container">
								<div class="row produtos-title">
									<div class="col-lg-9 center text-center">
										<h1>Winter Fós Gold Seca</h1>
									</div>
								</div>
								<div class="row produtos-content">
									<div class="block-content--img col-lg-5">
										<img src="./images/produtos/produto7.png">
									</div>
									<div class="container col-lg-7">
										<div class="block-content--text">
											<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
										</div>
									</div>
								</div>
								<div class="row produtos-link">
									<div class="text-center">
										<a href="">Mais Informações</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-link--bottom text-center oswald">
							<a href="">MONTE O SEU PROGRAMA DESEMPENHO MÁXIMO</a>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>




<?php include('rodapehome.php'); ?>
