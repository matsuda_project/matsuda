<div class="row modalOrcamento" id="ModalOrc">
	<div class="container center col-lg-12">
		<div class="row text-center">
			<div class="orc-title">
				<h1>MATSUDA FÓS PRIME</h1>
			</div>
			<div class="orc-subtitle">
				<p>Cada saco contém 25 kg</p>
			</div>
		</div>
		<div class="row orc-container text-center">
			<div class="orc-container--left left col-lg-9 col-xs-9 container">
				<p>Selecione a quantidade de sacos</p>
			</div>
			<div class="orc-container--right col-lg-3 col-xs-3 container">
				<div class="input-group spinner">
				    <input type="text" class="form-control" value="0">
			      	<button class="btn btn-top" type="button"><i class="fa fa-caret-up"></i></button>
			      	<button class="btn btn-bottom" type="button"><i class="fa fa-caret-down"></i></button>
			    </div>
			</div>
		</div>
		<div class="row orc-links col-lg-12 text-center">
			<div class="link-plus oswald">
				<a href="">SELECIONAR OUTROS PRODUTOS</a>
			</div>
			<div class="link-finalizar oswald">
				<a href="">FINALIZAR ORÇAMENTO</a>
			</div>
		</div>
	</div>
</div>