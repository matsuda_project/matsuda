<div class="row suplementos-container">
    <div class="container">
        <div class="col-lg-3 left">
            <div class="suplementos-block text-center container b1">
                <div class="img-block block-mais-carne block-img center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title text-center">
                        <div class="col-lg-6 center">
                            <h1>MAIS CARNE PASTO</h1>
                        </div>
                    </div>
                    <div class="block-desc text-center">
                        <div class="col-lg-6 center">
                            <p>Desempenho máximo para criação de gado a pasto</p>
                        </div>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">CONHEÇA</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 left">
            <div class="suplementos-block text-center container b2">
                <div class="img-block block-mais-carne2 block-img center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title text-center">
                        <div class="col-lg-6 center">
                            <h1>MAIS CARNE CONFINAMENTO</h1>
                        </div>
                    </div>
                    <div class="block-desc text-center">
                        <div class="col-lg-6 center">
                            <p>Desempenho máximo para criação de gado em confinamento</p>
                        </div>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 left">
            <div class="suplementos-block text-center container b3">
                <div class="img-block block-mais-bezerro block-img center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title text-center">
                        <div class="col-lg-6 center">
                            <h1>MAIS <span>BEZERRO</span></h1>
                        </div>
                    </div>
                    <div class="block-desc text-center">
                        <div class="col-lg-6 center">
                            <p>Desempenho máximo para criação de bezerros</p>
                        </div>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">ACESSE</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 left">
            <div class="suplementos-block text-center container b4">
                <div class="img-block block-mais-leite block-img center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title text-center">
                        <div class="col-lg-6 center">
                            <h1>MAIS <span>LEITE</span></h1>
                        </div>
                    </div>
                    <div class="block-desc text-center">
                        <div class="col-lg-6 center">
                            <p>Desempenho máximo para criação de gado leiteiro</p>
                        </div>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">CLIQUE AQUI</a>
                </div>
            </div>
        </div>     
    </div>
</div>