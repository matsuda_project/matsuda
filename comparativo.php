<?php include('topo-pages.php'); ?>
	<?php include('options.php'); ?>
	
	<div class="row col-lg-12 col-xs-12 page-comparativo">
		<div class="col-lg-10 col-xs-12 center">
			<div class="container center">
				<div class="col-lg-6 col-xs-12 left">
					<div class="col-lg-11 center">
						<div class="comparativo-container container">
							<div class="row comparativo-block text-center">
								<div class="row block-title">
									<h1>MATSUDA FÓS PRIME</h1>
								</div>
								<div class="block-img">
									<img src="./images/produto-desc.png">
								</div>
								<div class="row indicacao-block">
									<div class="row indicacao-block--title">
										<p>INDICAÇÃO</p>
									</div>
									<div class="row indicacao-block--text ">
										<div class="col-lg-8 center">
											<p>Suplemento Mineral proteico energético, pronto para uso, para rebanhos de cria, recria e engorda a pasto.</p>
										</div>
									</div>
								</div>
								<div class="row modo-usar-block">
									<div class="modo-usar-block--title">
										<p>MODO DE USAR</p>
									</div>
									<div class="modo-usar-block--text">
										<div class="col-lg-8 center">
											<p>Deve ser fornecido puro e de forma controlada em cochos apropriados com pelo menos 4 metros de comprimento para cada 10 cabeças</p>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="row informacoes-block container">
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>EMBALAGEM</p>
								</div>
								<div class="informacoes-block--img text-center">
									<img src="./images/embalagem.png">
								</div>
							</div>
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>PERÍODO</p>
								</div>
								<div class="informacoes-block--img text-center">
									<div class="col-lg-12 center">
										<img src="./images/chuvoso.png">	
									</div>
									<div class="col-lg-6 center ">
										<div class="info-desc text-center">
											<p><i>chuvoso</i></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>ESPÉCIE</p>
								</div>
								<div class="informacoes-block--img text-center">
									<div class="col-lg-12 center">
										<img src="./images/bovino.png">
									</div>
									<div class="col-lg-6 center ">
										<div class="info-desc text-center">
											<p><i>bovinos de corte</i></p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row consumo-estimado">
							<div class="col-lg-12 col-xs-12 text-center">
								<div class="row consumo-estimado--title">
									<p>CONSUMO ESTIMADO ANIMAL/ DIA</p>
								</div>
								<div class="row consumo-estimado--text">
									<p>250 gr/ dia</p>
								</div>
							</div>
						</div>
						<div class="row table-niveis">
							<div class="row table-container">
								<div class="col-lg-10 col-xs-10 text-center">
									<p>NÍVEIS DE GARANTIA</p>
								</div>
								<div class="col-lg-2 col-xs-2 text-center">
									<p>CMD*</p>
								</div>
							</div>
							<table class="table">
							    <tbody>
								    <tr>
							      		<td class="col-lg-8">Cálcio (máx.)</td>
							      		<td class="col-lg-2 text-center">30 g</td>
							      		<td class="col-lg-2 text-center green">7,5 g</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Cálcio (mín.)</td>
							      		<td class="col-lg-2 text-center">20 g</td>
							      		<td class="col-lg-2 text-center yellow">5 g</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Fosfóro (mín.)</td>
							      		<td class="col-lg-2 text-center">9.000 mg</td>
							      		<td class="col-lg-2 text-center pink">2.250 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Sódio (mín)</td>
							      		<td class="col-lg-2 text-center">37 g</td>
							      		<td class="col-lg-2 text-center blue">9,25 g</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Enxofre (mín.)</td>
							      		<td class="col-lg-2 text-center">16 g</td>
							      		<td class="col-lg-2 text-center green">4 g</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Magnésio (mín.)</td>
							      		<td class="col-lg-2 text-center">2.000 mg</td>
							      		<td class="col-lg-2 text-center green">500 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Cobalto (mín.)</td>
							      		<td class="col-lg-2 text-center">20 mg</td>
							      		<td class="col-lg-2 text-center pink">5mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Cobre (mín)</td>
							      		<td class="col-lg-2 text-center">150 mg</td>
							      		<td class="col-lg-2 text-center green">37,5 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Iodo (mín.)</td>
							      		<td class="col-lg-2 text-center">17 mg</td>
							      		<td class="col-lg-2 text-center pink">4,25 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Manganês (mín.)</td>
							      		<td class="col-lg-2 text-center">140 mg</td>
							      		<td class="col-lg-2 text-center pink">35 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Selênio (mín.)</td>
							      		<td class="col-lg-2 text-center">3 mg</td>
							      		<td class="col-lg-2 text-center pink">0,75 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Zinco (mín)</td>
							      		<td class="col-lg-2 text-center">600 mg</td>
							      		<td class="col-lg-2 text-center pink">150 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Ferro (mín.)</td>
							      		<td class="col-lg-2 text-center">100 mg</td>
							      		<td class="col-lg-2 text-center green">25 mg</td>
								    </tr> 
								    <tr>
							      		<td class="col-lg-8">Flúor (máx.)</td>
							      		<td class="col-lg-2 text-center">90 mg</td>
							      		<td class="col-lg-2 text-center green">22,5 mg</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">Proteína Bruta (mín.)</td>
							      		<td class="col-lg-2 text-center">200 g</td>
							      		<td class="col-lg-2 text-center green">50 g</td>
								    </tr>
								    <tr>
							      		<td class="col-lg-8">N.D.T (mín)</td>
							      		<td class="col-lg-2 text-center">700 g</td>
							      		<td class="col-lg-2 text-center green">175 g</td>
								    </tr>
								    
							    </tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12 left">
					<div class="col-lg-11 center">
						<div class="comparativo-container container">
							<div class="row comparativo-block text-center">
								<div class="row block-title">
									<h1>MATSUDA FÓS PRIME</h1>
								</div>
								<div class="block-img">
									<img src="./images/produto-desc.png">
								</div>
								<div class="row indicacao-block">
									<div class="row indicacao-block--title">
										<p>INDICAÇÃO</p>
									</div>
									<div class="row indicacao-block--text ">
										<div class="col-lg-8 center">
											<p>Suplemento Mineral proteico energético, pronto para uso, para rebanhos de cria, recria e engorda a pasto.</p>
										</div>
									</div>
								</div>
								<div class="row modo-usar-block">
									<div class="modo-usar-block--title">
										<p>MODO DE USAR</p>
									</div>
									<div class="modo-usar-block--text">
										<div class="col-lg-8 center">
											<p>Deve ser fornecido puro e de forma controlada em cochos apropriados com pelo menos 4 metros de comprimento para cada 10 cabeças</p>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="row informacoes-block container">
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>EMBALAGEM</p>
								</div>
								<div class="informacoes-block--img text-center">
									<img src="./images/embalagem.png">
								</div>
							</div>
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>PERÍODO</p>
								</div>
								<div class="informacoes-block--img text-center">
									<div class="col-lg-12 center">
										<img src="./images/chuvoso.png">	
									</div>
									<div class="col-lg-6 center ">
										<div class="info-desc text-center">
											<p><i>chuvoso</i></p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-xs-4 left">
								<div class="informacoes-block--title text-center">
									<p>ESPÉCIE</p>
								</div>
								<div class="informacoes-block--img text-center">
									<div class="col-lg-12 center">
										<img src="./images/bovino.png">
									</div>
									<div class="col-lg-6 center ">
										<div class="info-desc text-center">
											<p><i>bovinos de corte</i></p>
										</div>
									</div>
								</div>
							</div>
						</div>
							<div class="row consumo-estimado">
								<div class="col-lg-12 col-xs-12 text-center">
									<div class="row consumo-estimado--title">
										<p>CONSUMO ESTIMADO ANIMAL/ DIA</p>
									</div>
									<div class="row consumo-estimado--text">
										<p>250 gr/ dia</p>
									</div>
								</div>
							</div>
							<div class="row table-niveis">
								<div class="row table-container">
									<div class="col-lg-10 col-xs-10 text-center">
										<p>NÍVEIS DE GARANTIA</p>
									</div>
									<div class="col-lg-2 col-xs-2 text-center">
										<p>CMD*</p>
									</div>
								</div>
								<table class="table">
								    <tbody>
									    <tr>
								      		<td class="col-lg-8">Cálcio (máx.)</td>
								      		<td class="col-lg-2 text-center">30 g</td>
								      		<td class="col-lg-2 text-center green">7,5 g</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Cálcio (mín.)</td>
								      		<td class="col-lg-2 text-center">20 g</td>
								      		<td class="col-lg-2 text-center yellow">5 g</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Fosfóro (mín.)</td>
								      		<td class="col-lg-2 text-center">9.000 mg</td>
								      		<td class="col-lg-2 text-center pink">2.250 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Sódio (mín)</td>
								      		<td class="col-lg-2 text-center">37 g</td>
								      		<td class="col-lg-2 text-center blue">9,25 g</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Enxofre (mín.)</td>
								      		<td class="col-lg-2 text-center">16 g</td>
								      		<td class="col-lg-2 text-center green">4 g</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Magnésio (mín.)</td>
								      		<td class="col-lg-2 text-center">2.000 mg</td>
								      		<td class="col-lg-2 text-center green">500 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Cobalto (mín.)</td>
								      		<td class="col-lg-2 text-center">20 mg</td>
								      		<td class="col-lg-2 text-center pink">5mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Cobre (mín)</td>
								      		<td class="col-lg-2 text-center">150 mg</td>
								      		<td class="col-lg-2 text-center green">37,5 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Iodo (mín.)</td>
								      		<td class="col-lg-2 text-center">17 mg</td>
								      		<td class="col-lg-2 text-center pink">4,25 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Manganês (mín.)</td>
								      		<td class="col-lg-2 text-center">140 mg</td>
								      		<td class="col-lg-2 text-center pink">35 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Selênio (mín.)</td>
								      		<td class="col-lg-2 text-center">3 mg</td>
								      		<td class="col-lg-2 text-center pink">0,75 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Zinco (mín)</td>
								      		<td class="col-lg-2 text-center">600 mg</td>
								      		<td class="col-lg-2 text-center pink">150 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Ferro (mín.)</td>
								      		<td class="col-lg-2 text-center">100 mg</td>
								      		<td class="col-lg-2 text-center green">25 mg</td>
									    </tr> 
									    <tr>
								      		<td class="col-lg-8">Flúor (máx.)</td>
								      		<td class="col-lg-2 text-center">90 mg</td>
								      		<td class="col-lg-2 text-center green">22,5 mg</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">Proteína Bruta (mín.)</td>
								      		<td class="col-lg-2 text-center">200 g</td>
								      		<td class="col-lg-2 text-center green">50 g</td>
									    </tr>
									    <tr>
								      		<td class="col-lg-8">N.D.T (mín)</td>
								      		<td class="col-lg-2 text-center">700 g</td>
								      		<td class="col-lg-2 text-center green">175 g</td>
									    </tr>
								    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row col-lg-12 col-xs-12 consumo-estimado--legend">
				<div class="col-lg-10 center lengend-block">
					<div class="container center">
						<div class="lengend-block--title text-center">
							<h1>*CMD - Consumo Médio Diário - baseado na estimativa de consumo do produto pelo animal
								<a href="">CONHEÇA O CALCULO</a>
							</h1>
						</div>
						<div class="col-lg-4 center">
							<div class="lengend-block--container ">
								<div class="row legend-option">
									<div class="color left"></div>
									<div class="left">
										<p>Maior estimativa de consumo/ dia na comparação</p>
									</div>
								</div>
								<div class="row legend-option">
									<div class="color2 left"></div>
									<div class="left">
										<p>Menor estimativa de consumo/ dia na comparação</p>
									</div>
								</div>
								<div class="row legend-option">
									<div class="color3 left"></div>
									<div class="left">
										<p>Estimativas iguais de consumo/ dia na comparação</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row carrinho-orcamento">
				<div class="col-lg-6 col-xs-6">
					<div class="carrinho-orcamento--block text-center">
						<a href="">
							<div class="block-img">
								<img src="./images/icons/carrinho.png">
							</div>
							<div class="block-text col-lg-12 text-center">
								<p><i>Peça um Orçamento</i></p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-6 col-xs-6">
					<div class="carrinho-orcamento--block text-center">
						<a href="">
							<div class="block-img">
								<img src="./images/icons/carrinho.png">
							</div>
							<div class="block-text col-lg-12 text-center">
								<p><i>Peça um Orçamento</i></p>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="row return">
				<div class="col-lg-12 col-xs-12 return-link text-center">
					<a href="">Retornar</a>
				</div>
			</div>
		</div>
	







<?php include('rodapehome.php'); ?>