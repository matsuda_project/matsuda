<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Matsuda Portal</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/nora1.0.min.css">
        
        <link href="css/bootstrap.icon-large.min.css" rel="stylesheet">
 
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/_main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="menutopo">
      <div class="container container-fluid">
        <div class="row">
          <div class="col-lg-8 col-xs-12 col-sm-12 col-md-9 segmentos">
            <ul>
              <li><a href="#" class="colorGrupo">GRUPO MATSUDA</a></li>
              <li><a href="#" class="colorSementes">SEMENTES</a></li>
              <li><a href="#" class="colorNutricaoAnimal">NUTRIÇÃO ANIMAL</a></li>
              <li><a href="#" class="colorInoculantes">INOCULANTES</a></li>
              <li><a href="#" class="colorEquipamentos">EQUIPAMENTOS</a></li>
              <li><a href="#" class="colorEnergiaSolar">ENERGIA SOLAR</a></li>
              <li><a href="#" class="colorPeixes">PEIXES</a></li>
              <li><a href="#" class="colorPet">PET</a></li>
              <li><a href="#" class="colorSaudeAnimal">SAÚDE ANIMAL</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-xs-12 col-sm-6 col-md-3 redessociais container right">
            <ul>
              <li><a href="#"><img src="img/iconFace.jpg" alt="Facebook"></a></li>
              <li><a href="#"><img src="img/iconYoutube.png" alt="Youtube"></a></li>
              <li><a href="#"><img src="img/iconInstagran.jpg" alt="Instagran"></a></li>
              <li><a href="#"><img src="img/iconLinkedin.jpg" alt="Linkedin"></a></li>
              <li><a href="#"><img src="img/iconTwitter.jpg" alt="Twitter"></a></li>
              <li><a href="#"><img src="img/iconGoogle.jpg" alt="Google"></a></li>
              <li><a href="#"><img src="img/iconPin.jpg" alt="Pin"></a></li>
            </ul>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-5 linksIntranet header-pages">
            <div class="col-lg-3 col-xs-12 right">
              <ul>
                <li><a href="#"><img src="img/iconMail.png"> Email</a></li>
                <li><a href="#"><img src="img/iconIntranet.png"> Intranet</a></li>
                <li>
                  <div class="custom_select">
                      <div class="custom_select_selected text-center"><img src="./images/icons/flag-brasil.png">
                      </div>
                      <div class="custom_select_icon right">
                          <i class="fa fa-caret-down"></i>
                      </div>
                      <ul class="custom_select_options">
                          <li><a href="#" class="text-center"><img src="./images/icons/flag-brasil.png"></a></li>
                          <li><a href="#" class="text-center"><img src="./images/icons/flag-spain.png"></a></li>
                          <li><a href="#" class="text-center"><img src="./images/icons/flag-usa.png"></a></li>
                      </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-xs-12 menu-list mobile-hide tablet-hide">
      <div class="container center">
        <div class="menu-list--block ">
          <div class="block text-center">
            <div class="block-container">
              <div class="block-logo">
                <a href="index.php"><img src="./images/logo.png"></a>
              </div>
              <div class="block-icon">
                <div class="block-icon--img">
                  <img src="./images/icons/iconNutri.png">
                </div>
              </div>
              <div class="block-text pages">
                <p>Nutrição <span>animal</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="menu-list--nav pages right mobile-hide tablet-hide oswald">
          <nav>
            <ul>
              <li><a href="">NUTRIÇÃO ANIMAL</a></li><div class="skew positive"></div>
              <li><a href="">DESEMPENHO MÁXIMO</a></li><div class="skew positive"></div>
              <li><a href="">VÍDEOS</a></li><div class="skew positive"></div>
              <li><a href="">ARTIGOS TÉCNICOS</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-default mobile-show tablet-show" role="navigation">
      <div class="row container container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" id="mostraMenu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="img/logoTopo.png" /> </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="">
          <ul class="nav navbar-nav navbar-right">
            <li class="visible-xs">
              <a href="#" class="linkvideo linkNossosTelefoes">
                <span class="linkmenu">NOSSOS TELEFONES</span>
                <span class="caret"><img src="img/iconTelMenu.png" alt=""></span>
              </a>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="linkmenu">A MATSUDA</span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">Sementes</a></li>
                <li><a href="#">Nutrição Animal</a></li>                
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="linkmenu">PRODUTOS</span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">Sementes</a></li>
                <li><a href="#">Nutrição Animal</a></li>
                <li><a href="#">Inoculantes</a></li>                
                <li><a href="#">Equipamentos</a></li>
                <li><a href="#">Energia Solar</a></li>
                <li><a href="#">Peixes</a></li>
                <li><a href="#">Pet</a></li>
                <li><a href="#">Saúde Animal</a></li>
              </ul>
            </li>
            <li>
              <a href="#" class="linkvideo">
                <span class="linkmenu">VIDEOS</span>
                <span class="caret"></span>
              </a>
            </li>
            <li class="ondecomprar"><a href="#">Onde Comprar?</a></li>
          </ul>         

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container container-fluid-fluid -->
    </nav>
      </div>
    </div>