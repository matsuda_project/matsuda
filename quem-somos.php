<?php include('topo-pages.php'); ?>
	<div class="col-lg-12 col-xs-12 nutricao-animal">
		<div class="container center">
			<div class="nutri-block">
				<div class="col-lg-6 col-xs-12 left">
					<div class="nutri-block--content container">
						<div class="nutri-block--title container">
							<h1>NUTRIÇÃO ANIMAL</h1>
						</div>
						<div class="nutri-block--text">
							<p>Com a crescente demanda do mercado mundial  por carne bovina de qualidade, proveniente de animais criados a pasto, as perspectivas para a pecuária brasileira são imensas. A  carne bovina é um alimento altamente saudável e nutritivo, sendo uma das mais importantes fontes de minerais como zinco, ferro, selênio, aminoácidos essenciais, vitaminas do complexo B e ácido linoléico (CLA) que são essenciais para nutrir a humanidade.</p>

							<p>O  fornecimento de carne para o mercado mundial está se restringindo a poucos países. Estudos da FAO, órgão da ONU que acompanha a agricultura e pecuária, apontam que em 2040,  60% da carne comercializada para abastecer o mercado mundial será brasileira. Não há como abastecer o mundo sem pensar em carne do Brasil.</p>

							<p>Isso exigirá dos produtores brasileiros profissionalismo e somente com uso de tecnologias práticas e  economicamente viáveis, esse crescimento sustentável da pecuária brasileira será possível. Estas tecnologias envolvem: nutrição correta; controle sanitário eficiente melhoramento  genético.</p>

							<p>Toda criação animal começa pela alimentação correta e se não houver um manejo nutricional eficiente e viável não há como esta atividade ser sustentável. Nenhum sistema é mais eficiente que a bovinocultura a pasto,  com  as devidas suplementações necessárias que corrijam as deficiências nutricionais que ocorrem durante cada época do ano. Não há como possuir rentabilidade em pecuária seja de corte ou de leite sem a adoção de tecnologias simples e eficientes que promovam a eficiência do rebanho, e assim a sua viabilidade econômica.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12 right">
					<div class="nutri-block--movie container">
						<div class="nutri-block--movie-title container">
							<h1>VÍDEO INSTITUCIONAL</h1>
						</div>
						<iframe width="100%" height="336" src="https://www.youtube.com/embed/URaJ6Dc7cEo?ecver=1" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>













<?php include('rodapehome.php'); ?>