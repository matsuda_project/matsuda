<div class="row col-lg-12 col-xs-12 col-md-12 fazendas-cadastradas">
	<div class="col-lg-4 col-xs-12 container">
		<div class="fazendas-cadastradas--container text-center oswald">
			<div class="row fazendas-cadastradas--block">
				<div class="row fazenda-name">
					<h1>Fazenda 3 Ilhas</h1>
				</div>
				<div class="row fazenda-local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
	      	<div class="custom_select">
	          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
	          	</div>
	          	<div class="custom_select_icon right">
	              	<i class="fa fa-caret-down"></i>
	         	</div>
	          	<ul class="custom_select_options">
	              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
	              	<li><a href="#" class="text-center"><i>Lotes</i></a></li>
	              	<li><a href="#" class="text-center"><i>Renomear</i></a></li>
	              	<li><a href="#" class="text-center"><i>Alterar Município</i></a></li>
	              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
	          	</ul>
	        </div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12 container">
		<div class="fazendas-cadastradas--container text-center oswald">
			<div class="row fazendas-cadastradas--block">
				<div class="row fazenda-name">
					<h1>Fazenda União</h1>
				</div>
				<div class="row fazenda-local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
	      	<div class="custom_select">
	          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
	          	</div>
	          	<div class="custom_select_icon right">
	              	<i class="fa fa-caret-down"></i>
	         	</div>
	          	<ul class="custom_select_options">
	              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
	              	<li><a href="#" class="text-center"><i>Lotes</i></a></li>
	              	<li><a href="#" class="text-center"><i>Renomear</i></a></li>
	              	<li><a href="#" class="text-center"><i>Alterar Município</i></a></li>
	              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
	          	</ul>
	        </div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12 container">
		<div class="fazendas-cadastradas--container text-center oswald">
			<div class="row fazendas-cadastradas--block">
				<div class="row fazenda-name">
					<h1>Fazenda 3 Ilhas</h1>
				</div>
				<div class="row fazenda-local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
	      	<div class="custom_select">
	          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
	          	</div>
	          	<div class="custom_select_icon right">
	              	<i class="fa fa-caret-down"></i>
	         	</div>
	          	<ul class="custom_select_options">
	              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
	              	<li><a href="#" class="text-center"><i>Lotes</i></a></li>
	              	<li><a href="#" class="text-center"><i>Renomear</i></a></li>
	              	<li><a href="#" class="text-center"><i>Alterar Município</i></a></li>
	              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
	          	</ul>
	        </div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12 container">
		<div class="fazendas-cadastradas--container text-center oswald">
			<div class="row fazendas-cadastradas--block">
				<div class="row fazenda-name">
					<h1>Fazenda 3 Ilhas</h1>
				</div>
				<div class="row fazenda-local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
	      	<div class="custom_select">
	          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
	          	</div>
	          	<div class="custom_select_icon right">
	              	<i class="fa fa-caret-down"></i>
	         	</div>
	          	<ul class="custom_select_options">
	              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
	              	<li><a href="#" class="text-center"><i>Lotes</i></a></li>
	              	<li><a href="#" class="text-center"><i>Renomear</i></a></li>
	              	<li><a href="#" class="text-center"><i>Alterar Município</i></a></li>
	              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
	          	</ul>
	        </div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12 container">
		<div class="fazendas-cadastradas--container text-center oswald">
			<div class="row fazendas-cadastradas--block">
				<div class="row fazenda-name">
					<h1>Fazenda 3 Ilhas</h1>
				</div>
				<div class="row fazenda-local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
	      	<div class="custom_select">
	          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
	          	</div>
	          	<div class="custom_select_icon right">
	              	<i class="fa fa-caret-down"></i>
	         	</div>
	          	<ul class="custom_select_options">
	              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
	              	<li><a href="#" class="text-center"><i>Lotes</i></a></li>
	              	<li><a href="#" class="text-center"><i>Renomear</i></a></li>
	              	<li><a href="#" class="text-center"><i>Alterar Município</i></a></li>
	              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
	          	</ul>
	        </div>
		</div>
	</div>
</div>