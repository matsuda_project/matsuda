    
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/ResponsiveSlides/responsiveslides.min.js"></script>
    <link href="js/ResponsiveSlides/responsiveslides.css" rel="stylesheet" type="text/css">

    <!-- bxSlider Javascript file -->
    <script src="js/bxslider/jquery.bxslider.min.js"></script>
    <!-- bxSlider CSS file -->
    <link href="js/bxslider/jquery.bxslider.css" rel="stylesheet" />

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/animation.js"></script>
    <script src="js/menulateral.js"></script>
    <script type="text/javascript" src="js/nora.1.0.min.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>
