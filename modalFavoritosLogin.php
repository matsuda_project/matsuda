<div class="modalFavoritosLogin" id="ModalFavoritosLogin">
	<div class="container center">
		<div class="login-form col-lg-12 col-xs-12">
			<div class="login-form--title text-center">
				<p>Acesse sua área de cliente</p>
			</div>
			<form>
				<div class="row login-form--container">
					<div class="left col-lg-2 col-xs-3">
						<p>Email:</p>
					</div>
					<div class="right col-lg-10 col-xs-9">
						<input type="text" name="txtEmail">
					</div>
				</div>
				<div class="row login-form--container block">
					<div class="left col-lg-2 col-xs-3">
						<p>Senha:</p>
					</div>
					<div class="right col-lg-10 col-xs-9">
						<input type="password" name="txtSenha">
					</div>
				</div>
				<div class="btn-login text-center oswald">
					<button>ACESSAR</button>
				</div>
				<div class="link-cadastro text-center">
					<a href="">Não sou cadastrado</a>
				</div>
			</form>
		</div>
	</div>
</div>