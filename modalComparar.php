<div class="modalComparar" id="ModalComparar">
	<div class="comparar-block col-lg-12">
		<div class="col-lg-10 col-xs-11 center">
			<div class="comparar-block--title text-center">
				<h1>COMPARAÇÃO DE PRODUTOS</h1>
			</div>
			<div class="comparar-block--search">
				<input type="text" name="" placeholder="Digite acima o nome do produto para comparar">
			</div>
			<div class="row comparar-container">
				<div class="col-lg-12 center">
					<div class="col-lg-5 col-xs-5 left">
						<div class="container-img1 text-center">
							<img src="./images/produtos/produto2.png">
						</div>
						<div class="container-title text-center oswald">
							<p>MATSUDA FÓS PRIME</p>
						</div>
					</div>
					<div class="col-lg-2 col-xs-2">
						<div class="container-icon text-center">
							<img src="./images/icons/icon-compare.png">
						</div>
					</div>
					<div class="col-lg-5 col-xs-5 right">
						<div class="container-img2 text-center">
							<img src="./images/produtos/produto3.png">
						</div>
						<div class="container-title text-center oswald">
							<p>MATSUDA PHÓS VERÃO ACABAMENTO</p>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12">
						<div class="row comparar-button">
							<div class="text-center oswald">
								<button>COMPARAR</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>