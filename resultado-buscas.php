<?php include('topo-pages.php'); ?>
	<div class="row col-lg-12 col-xs-12 resultado-buscas">
		<div class="container center">
			<div class="row resultado-container">
				<div class="row resultado-title ">
					<p>RESULTADO DE BUSCA</p>
				</div>
				<div class="row resultado-assunto">
					<p>Resultado de busca para a palavra: <span><i>Fós Reprodução</i></span></p>
				</div>
				<div class="col-lg-6 col-xs-12">
					<div class="resultado-container--block col-lg-11 col-md-11 center">
						<div class="row block-title text-center">
							<p>PRODUTOS</p>
						</div>
						<div class="row block-produtos--align">
							<div class="block-produtos--container">
								 <div class="block-content col-lg-6 col-xs-6">
								 	<div class="block-content--img center">
								 		<img src="./images/produtos/produto2.png">
								 	</div>
								 	<div class="block-content--text text-center">
								 		<span><i><a href="">Fós Reprodução</a></i></span>
								 	</div>
								 </div>
							</div>
							<div class="block-produtos--container">
								 <div class="block-content col-lg-6 col-xs-6">
								 	<div class="block-content--img center">
								 		<img src="./images/produtos/produto2.png">
								 	</div>
								 	<div class="block-content--text text-center">
								 		<span><i><a href="">Fós Reprodução</a></i></span>
								 		<span><i>Embryo</i></span>
								 	</div>
								 </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12">
					<div class="resultado-container--block col-lg-11 col-md-11 center">
						<div class="row block-title text-center">
							<p>VÍDEOS</p>
						</div>
						<div class="row block-produtos--align">
							<div class="col-lg-11 col-md-11 center">
								<div class="row block-movie--container">
									<div class="text-center">
										<a href="">
											<div class="movie-icon left col-lg-1 col-xs-1">
											 	<i class="fa fa-file-video-o"></i>
											</div>
											<div class="movie-text left col-lg-11 col-xs-11">
												<i><span>Fós Reprodução</span> é utilizado na Fazenda Santo Antônio Fós Reprodução é utilizado na Fazenda Santo Antônio</i>
											</div>
										</a>
									</div>
								</div>
								<div class="row block-movie--container">
									<div class="text-center">
										<a href="">
											<div class="movie-icon left col-lg-1 col-xs-1">
											 	<i class="fa fa-file-video-o"></i>
											</div>
											<div class="movie-text left col-lg-11 col-xs-11">
												<i><span>Fós Reprodução</span> é utilizado na Fazenda Santo Antônio Fós Reprodução é utilizado na Fazenda Santo Antônio</i>
											</div>
										</a>
									</div>
								</div>
								<div class="row block-movie--container">
									<div class="text-center">
										<a href="">
											<div class="movie-icon left col-lg-1 col-xs-1">
											 	<i class="fa fa-file-video-o"></i>
											</div>
											<div class="movie-text left col-lg-11 col-xs-11">
												<i><span>Fós Reprodução</span> é utilizado na Fazenda Santo Antônio Fós Reprodução é utilizado na Fazenda Santo Antônio</i>
											</div>
										</a>
									</div>
								</div>
								<div class="row block-movie--container">
									<div class="text-center">
										<a href="">
											<div class="movie-icon left col-lg-1 col-xs-1">
											 	<i class="fa fa-file-video-o"></i>
											</div>
											<div class="movie-text left col-lg-11 col-xs-11">
												<i><span>Fós Reprodução</span> é utilizado na Fazenda Santo Antônio Fós Reprodução é utilizado na Fazenda Santo Antônio</i>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12">
					<div class="resultado-container--block col-lg-11 col-md-11">
						<div class="row block-title text-center">
							<p>PROGRAMA DESEMPENHO MÁXIMO</p>
						</div>
						<div class="row block-produtos--align">
							<div class="col-lg-10 center">
								<div class="row block-desempenho--container">
									<a href="">
										<div class="block-icon col-lg-1 col-xs-1">
											<i><i class="fa fa-plus"></i></i>
										</div>
										<div class="block-text col-lg-11 col-xs-11">
											<p><i>Período Seco, prevenir ou remediar? <span>Mais Bezerro</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-desempenho--container">
									<a href="">
										<div class="block-icon col-lg-1 col-xs-1">
											<i><i class="fa fa-plus"></i></i>
										</div>
										<div class="block-text col-lg-11 col-xs-11">
											<p><i>Alimentação de equídeos com volumoso <span>Mais Carne Pasto</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-desempenho--container">
									<a href="">
										<div class="block-icon col-lg-1 col-xs-1">
											<i><i class="fa fa-plus"></i></i>
										</div>
										<div class="block-text col-lg-11 col-xs-11">
											<p><i>Suplementação de bovinos criados a pasto na época seca do ano <span>Programa Desempenho Máximo</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-desempenho--container">
									<a href="">
										<div class="block-icon col-lg-1 col-xs-1">
											<i><i class="fa fa-plus"></i></i>
										</div>
										<div class="block-text col-lg-11 col-xs-11">
											<p><i>A importância de uma boa Suplementação Mineral <span>Mais Carne Confinamento</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12">
					<div class="resultado-container--block col-lg-11 col-md-11 artigos">
						<div class="row block-title text-center">
							<p>ARTIGOS TÉCNICOS</p>
						</div>
						<div class="row block-produtos--align">
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Período Seco, prevenir ou remediar? <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Alimentação de equídeos com volumoso <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Suplementação de bovinos criados a pasto na época seca do ano <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>A importância de uma boa Suplementação Mineral <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Período Seco, prevenir ou remediar? <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Alimentação de equídeos com volumoso <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>Suplementação de bovinos criados a pasto na época seca do ano <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-lg-10 center">
								<div class="row block-artigos--container">
									<a href="">
										<div class="artigos-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-file-text" ></i>
										</div>
										<div class="artigos-text col-lg-11 col-xs-11 left">
											<p><i>A importância de uma boa Suplementação Mineral <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12">
					<div class="resultado-container--block col-lg-11 col-md-11 col-xs-12">
						<div class="row block-title text-center">
							<p>NOTÍCIA</p>
						</div>
						<div class="col-lg-10 col-xs-12 center">
							<div class="row block-produtos--align">
								<div class="block-noticias--container">
									<a href="">
										<div class="noticias-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-newspaper-o"></i>
										</div>
										<div class="noticias-text col-lg-11 col-xs-11 left">
											<p><i>Período Seco, prevenir ou remediar? <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-10 col-xs-12 center">
							<div class="row block-produtos--align">
								<div class="block-noticias--container">
									<a href="">
										<div class="noticias-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-newspaper-o"></i>
										</div>
										<div class="noticias-text col-lg-11 col-xs-11 left">
											<p><i>Alimentação de equídeos com volumoso <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-10 col-xs-12 center">
							<div class="row block-produtos--align">
								<div class="block-noticias--container">
									<a href="">
										<div class="noticias-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-newspaper-o"></i>
										</div>
										<div class="noticias-text col-lg-11 col-xs-11 left">
											<p><i>Suplementação de bovinos criados a pasto na época seca do ano <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-10 col-xs-12 center">
							<div class="row block-produtos--align">
								<div class="block-noticias--container">
									<a href="">
										<div class="noticias-icon col-lg-1 col-xs-1 left">
											<i class="fa fa-newspaper-o"></i>
										</div>
										<div class="noticias-text col-lg-11 col-xs-11 left">
											<p><i>A importância de uma boa Suplementação Mineral <span>Nutrição Animal</span></i></p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row return">
					<div class="col-lg-12 col-xs-12 return-link text-center">
						<a href=""><i>Retornar</i></a>
					</div>
				</div>
			</div>
		</div>
	</div>




<?php include('rodapehome.php'); ?>