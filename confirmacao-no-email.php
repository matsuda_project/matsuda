<?php include('topo-pages.php'); ?>

	<div class="row col-lg-12 col-xs-12 email-confirmacao">
		<div class="container center">
			<div class="confirmacao-block text-center">
				<div class="confirmacao-title">
					<h1>RECEBEMOS OS SEUS DADOS</h1>
				</div>
				<div class="confirmacao-content">
					<p>Acesse seu e-mail e confirme seu cadastro</p>
				</div>
				<div class="confirmacao-img">
					<img src="./images/email-confirmacao.png">
				</div>
				<div class="row col-lg-12 col-xs-12 confirmacao-emails">
					<div class="container center">
						<ul>
							<li>
								<div class="email-block">
									<div class="email-block--img--yahoo">
										<a href=""><img src="./images/mail-yahoo.png"></a>
									</div>
								</div>
							</li>
							<li>
								<div class="email-block">
									<div class="email-block--img--gmail">
										<a href=""><img src="./images/mail-gmail.png"></a>
									</div>
								</div>
							</li>
							<li>
								<div class="email-block">
									<div class="email-block--img--hotmail">
										<a href=""><img src="./images/mail-hotmail.png"></a>
									</div>
								</div>
							</li>
							<li>
								<div class="email-block">
									<div class="email-block--img--uol">
										<a href=""><img src="./images/mail-uol.png"></a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>








<?php include('rodapehome.php'); ?>