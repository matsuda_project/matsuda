$(document).ready(function(){
    ContainerHeight();
    ContainerHeightServices();
    CustomSelect();
    ContainerHeightListHeader();
    VerifyMenu();

    $("#fechaFlutuante").on('click', function(){
        $(".header_row ").removeClass('in');
        if($("#mostraMenu").hasClass('navbar-toggle')){
            $('.navbar-toggle').css({'display':'block'});
        }
    });

    $("#mostraMenu").on('click', function(){
        $('.navbar-toggle').css({'display':'none'});
    });

    $("#menu-icon").on('click', function(){
        if($(".nav-mobile--container").hasClass('open')){
             $(".nav-mobile--container").removeClass('open');
        }
        else{
            $(".nav-mobile--container").addClass('open');
        }
    });
    $("#close-icon").on('click', function(){
        if($(".nav-mobile--container").hasClass('open')){
             $(".nav-mobile--container").removeClass('open');
        }
    });

    $(window).scroll(function(){
        if($('.nav-mobile--container') != null){
            $('.nav-mobile--container').removeClass('open');
        }
    });

    if(document.getElementsByClassName("banner-carousel") != null){
        var carousel = $(".banner-carousel");
        carousel.owlCarousel({
            items:1,
            nav: true,
            navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
            responsiveClass:true,
            autoplay: true, 
            autoplayTimeout: 4000,
            loop: true,
            animateIn: "fadeIn",
            animateOut: "fadeOut"
        });
    } 

    if(document.getElementsByClassName("emdestaqueCarousel") != null){
        var carouselDestaque = $(".emdestaqueCarousel");
        carouselDestaque.owlCarousel({
            items:5,
            responsiveClass:true,
            animateIn: "fadeIn",
            animateOut: "fadeOut",
            responsive:{
                320:{
                    items:1,
                },
                768:{
                    items:3,
                },
                1024:{
                    items:4,
                }
            }
        });
    }

    $(window).scroll(function(){
        var top = ($(window).scrollTop());
        if(top >= 150){
            $('.header-list').addClass('header-scroll');
            $('.header-info').css({'display':'none'});
            $('.header-mobile').addClass('header-mobile--scroll');
        }
        else
        {
            $('.header-info').css({'display':'block'});
            $('.header-list').removeClass('header-scroll');
            if(top = 150){
                $('.header-mobile').removeClass('header-mobile--scroll');
            }
        }
    });


     $("#BtnModalOrc").on('click', function(e){
        e.preventDefault();
        var modalOrc    = new Modal(),
            clone       = $("#ModalOrc").clone();
        modalOrc.setContainer(clone[0]);
        modalOrc.setClassModal("modalOrcContainer");
        modalOrc.setClassModalContainer("open");
        modalOrc.setClassModalContainer("col-lg-3");
        modalOrc.setClassModalContainer("col-xs-11");
        modalOrc.setClassModalContainer("center");
        modalOrc.showModal();
    });   


     $("#BtnFavoritos").on('click', function(e){
        e.preventDefault();
        var modalFav = new Modal(),
        cloneFav = $("#ModalFavoritosLogin").clone();
        modalFav.setContainer(cloneFav[0]);
        modalFav.setClassModal("open");
        modalFav.setClassModal("modalFavLogContainer");
        modalFav.setClassModalContainer("col-lg-4");
        modalFav.setClassModalContainer("col-xs-11");
        modalFav.setClassModalContainer("open");
        modalFav.setClassModalContainer("center");
        modalFav.showModal();
    });

     $("#BtnLogado").on('click', function(e){
        e.preventDefault();
        var modalLogado = new Modal(),
        cloneLog = $("#ModalFavoritosLogado").clone();
        modalLogado.setContainer(cloneLog[0]);
        modalLogado.setClassModal("open");
        modalLogado.setClassModal("modalFavLogado");
        modalLogado.setClassModalContainer("col-lg-3");
        modalLogado.setClassModalContainer("col-xs-11");
        modalLogado.setClassModalContainer("open");
        modalLogado.setClassModalContainer("center");
        modalLogado.showModal();
    });
     $("#BtnComparar").on('click', function(e){
        e.preventDefault();
        var modalLogado = new Modal(),
        cloneLog = $("#ModalComparar").clone();
        modalLogado.setContainer(cloneLog[0]);
        modalLogado.setClassModal("open");
        modalLogado.setClassModal("modalCompararBlock");
        modalLogado.setClassModalContainer("col-lg-3");
        modalLogado.setClassModalContainer("col-xs-11");
        modalLogado.setClassModalContainer("open");
        modalLogado.setClassModalContainer("center");
        modalLogado.showModal();
    });


     $("#btnCadastroProp").on('click', function(e){
        e.preventDefault();
        var modalLogado = new Modal(),
        cloneLog = $("#ModalCadProp").clone();
        modalLogado.setContainer(cloneLog[0]);
        modalLogado.setClassModal("open");
        modalLogado.setClassModal("modalCadProp");
        modalLogado.setClassModalContainer("col-lg-4");
        modalLogado.setClassModalContainer("col-xs-11");
        modalLogado.setClassModalContainer("open");
        modalLogado.setClassModalContainer("center");
        modalLogado.showModal();
    });

     $("#btnCadLotes").on('click', function(e){
        e.preventDefault();
        var modalLogado = new Modal(),
        cloneLog = $("#ModalCadLotes").clone();
        modalLogado.setContainer(cloneLog[0]);
        modalLogado.setClassModal("open");
        modalLogado.setClassModal("modalCadLotes");
        modalLogado.setClassModalContainer("col-lg-6");
        modalLogado.setClassModalContainer("col-xs-11");
        modalLogado.setClassModalContainer("open");
        modalLogado.setClassModalContainer("center");
        modalLogado.showModal();
    });
     $("#btnTrocarProduto").on('click', function(e){
        e.preventDefault();
        var modalLogado = new Modal(),
        cloneLog = $("#ModalTroca").clone();
        modalLogado.setContainer(cloneLog[0]);
        modalLogado.setClassModal("open");
        modalLogado.setClassModal("modalTrocaProd");
        modalLogado.setClassModalContainer("col-lg-6");
        modalLogado.setClassModalContainer("col-xs-11");
        modalLogado.setClassModalContainer("open");
        modalLogado.setClassModalContainer("center");
        modalLogado.showModal();
    });

     (function ($) {
        $('.spinner .btn:first-of-type').on('click', function() {
            $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
        });
        $('.spinner .btn:last-of-type').on('click', function() {
            $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
        });
    })(jQuery);

});
function ContainerHeight(){
    var container = document.getElementsByClassName("control-suplementos");
    var size = 0;
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        if(size == 0){
            size = sizePost;
        }
        else{
            if(sizePost > size){
                size = sizePost;
            }
        }
    }
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        post.style.height = size + "px";
    }   
}

function ContainerHeightServices(){
    var container = document.getElementsByClassName("services-block-container");
    var size = 0;
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        if(size == 0){
            size = sizePost;
        }
        else{
            if(sizePost > size){
                size = sizePost;
            }
        }
    }
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        post.style.height = size + "px";
    }   
}
function ContainerHeightListHeader(){
    var container = document.getElementsByClassName("desempenho-list--block");
    var size = 0;
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        if(size == 0){
            size = sizePost;
        }
        else{
            if(sizePost > size){
                size = sizePost;
            }
        }
    }
    for(var i = 0; i < container.length; i++){
        var post = container[i],
        sizePost = post.clientHeight;
        post.style.height = size + "px";
    }   
}

function CustomSelect() {
    var container = ".custom_select",
        selected_selector = ".custom_select_selected",
        obj = $(container);
    
    if (obj.length) {
        var selected = obj.find(selected_selector);
        var options = obj.find("ul").children();

        $(this).css("z-index", parseInt(999));

        $(selected).click(function() {
            options.parent().toggle();
        });

        $.each(options, function(k1, v1) {
            $(this).click(function() {
                selected.html($(this).html());
                options.parent().toggle();
            });
        });
    }
}

function VerifyMenu(){
    if($('.menu-sub').hasClass("active")){
        $('.menu-descricao').removeClass("active");
    }
}