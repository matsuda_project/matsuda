/*
########################################
	MODAL
########################################
*/
	var Modal = function(){
		this.body 				= document.getElementsByTagName("body")[0];
		this.modal 				= document.createElement("div");
		this.modalContainer 	= document.createElement("div");
		this.header 			= document.createElement("div");
		this.btnClose 			= document.createElement("button");
		this.iconClose 			= document.createElement("i");
		this.list  				= document.createElement("div");
		this.listItem			= document.createElement("div");
		this.cbOnClose 			= null;
		this.cbOnload 			= null;
		this.init();
	}
	/*
	-------------------------
		INIT
	-------------------------
	*/
		Modal.prototype.init = function(){
			this.modal.setAttribute("class", "modal-nora layout-control--js layout-height--full layout-container--center");
			this.modal.setAttribute("id", "modal");
			this.modalContainer.setAttribute("class", "modal-container layout-container");
			this.header.setAttribute("class", "modal-container--header");
			this.btnClose.setAttribute("class", "modal-action modal-control--close");
			this.iconClose.setAttribute("class", "fa fa-close");
			this.btnClose.appendChild(this.iconClose);
			this.modalContainer.appendChild(this.btnClose);
		}
	/*
	-------------------------
		//INIT
	-------------------------
	*/
	/*
	////////////////////////////////
		CARREGAR
	////////////////////////////////
	*/
		/*
		--------------------------------
			OPTION
		--------------------------------
		*/
			Modal.prototype.carregarOption = function(option){
				if(option.header != null){
					this.createHeader(option.header);
				}
				if(option.iFrame != null){
					this.createIFrame(option.iFrame);
				}
				if(option.onClose != null){
					this.setOnClose(option.onClose);
				}
				this.showModal();
			}
		/*
		--------------------------------
			//OPTION
		--------------------------------
		*/
		/*
		--------------------------------
			UX
		--------------------------------
		*/
			Modal.prototype.carregarUx = function(){
				var modal 		= this,
					btnClose 	= document.getElementsByClassName("modal-control--close"),
					count 		= btnClose.length;
				for(var i = 0; i < count; i++){					
					btnClose[i].addEventListener("click", function(){
						modal.fecharModal();
					});
				}
				document.addEventListener("keydown", function(e){
					if(e.keyCode == 27){
						modal.fecharModal();
					}
				});
			}
		/*
		--------------------------------
			//UX
		--------------------------------
		*/
	/*
	////////////////////////////////
		//CARREGAR
	////////////////////////////////
	*/
	/*
	////////////////////////////////
		SET
	////////////////////////////////
	*/
		/*
		-----------------------------
			CLASS
		-----------------------------
		*/
			Modal.prototype.setClassModal = function(data){
				this.modal.classList.add(data);
			}
			Modal.prototype.setClassModalContainer = function(data){
				this.modalContainer.classList.add(data);
			}
		/*
		-----------------------------
			//CLASS
		-----------------------------
		*/
		/*
		-----------------------------
			MODAL
		-----------------------------
		*/
			Modal.prototype.setContainer = function(data){
				this.modalContainer.appendChild(data);
			}
		/*
		-----------------------------
			//MODAL
		-----------------------------
		*/
		/*
		-----------------------------
			ON LOAD
		-----------------------------
		*/
			Modal.prototype.setOnLoad = function(data){
				this.cbOnload = data;
			}
		/*
		-----------------------------
			//ON LOAD
		-----------------------------
		*/
		/*
		-----------------------------
			ON CLOSE
		-----------------------------
		*/
			Modal.prototype.setOnClose = function(data){
				this.cbOnClose = data;
			}
		/*
		-----------------------------
			//ON CLOSE
		-----------------------------
		*/
	/*
	////////////////////////////////
		//SET
	////////////////////////////////
	*/
	/*
	////////////////////////////////
		CREATE
	////////////////////////////////
	*/
		/*
		-------------------------------
			HEADER
		-------------------------------
		*/
			Modal.prototype.createHeader = function(data){
				if(data.title != null){
					var modal 		= this.modalContainer,
						header 		= this.header,
						title 		= document.createElement("h2");
					if(data.subTitle != null){
						var subTitle = document.createElement("p");
						title.innerHTML = data.title;
						subTitle.innerHTML = data.subTitle;
						header.appendChild(title);
						header.appendChild(subTitle);
						modal.appendChild(header);
					}else{
						title.innerHTML = data.title;
						header.appendChild(title);
						modal.appendChild(header);
					}
				}else{
					this.feedBack("error", 2);
				}
			}
		/*
		-------------------------------
			//HEADER
		-------------------------------
		*/
		/*
		-------------------------------
			IFRAME
		-------------------------------
		*/
			Modal.prototype.createIFrame = function(data){
				if(data.url != null){
					var modal 		= this.modalContainer,
						iframe 		= document.createElement("iframe");
					iframe.setAttribute("src", data.url);
					iframe.setAttribute("frameborder", 0);
					if(data.type != null){
						switch(data.type){
							case "website":
								var device = getDevice();
								switch(device){
									case "desktop":
										iframe.setAttribute("width", 320);
										iframe.setAttribute("height", 480);
										break;
									case "notebook":
										iframe.setAttribute("width", 320);
										iframe.setAttribute("height", 480);
										break;
								}
								break;
							case "video":
								var device = getDevice();
								switch(device){
									case "desktop":
										iframe.setAttribute("width", 640);
										iframe.setAttribute("height", 360);
										break;
									case "notebook":
										iframe.setAttribute("width", 640);
										iframe.setAttribute("height", 360);
										break;
								}
								break;
						}
					}
					modal.appendChild(iframe);
				}else{
					this.feedBack("error", 3);
				}
			}
		/*
		-------------------------------
			//IFRAME
		-------------------------------
		*/
		/*
		///////////////////////////////
			IMAGE
		///////////////////////////////
		*/
			/*	
			====================================
				LIST
			====================================
			*/
				Modal.prototype.createListImage = function(){
					var list 		= this.list,
						items 		= this.listItem,
						container 	= this.container;
					console.log(this.list);
					items.setAttribute("class", "modal-carousel--list");
					list.setAttribute("class", "modal-carousel");
					list.setAttribute("id", "modal-carousel--app");
					list.appendChild(items);
					container.appendChild(list);
				}
				/*
				---------------------------------
					IMAGE
				---------------------------------
				*/
					Modal.prototype.createImageItem = function(element){
						var listItem 		= this.listItem,
							item 		= document.createElement("div"),
							image 		= document.createElement("img"),
							image_src 	= element.getAttribute("src");
						item.setAttribute("class", "item modal-carousel--item");
						image.setAttribute("src", image_src);
						item.appendChild(image);
						listItem.appendChild(item);
					}
				/*
				---------------------------------
					//IMAGE
				---------------------------------
				*/
			/*	
			====================================
				//LIST
			====================================
			*/
			/*
			------------------------------------
				SINGLE
			------------------------------------
			*/
				Modal.prototype.createImage = function(element){
					var container 	= this.modalContainer,
						image 		= document.createElement("img"),
						image_src	= element.getAttribute("src");
					image.setAttribute("src", image_src);
					container.appendChild(image);
				}
			/*
			------------------------------------
				//SINGLE
			------------------------------------
			*/
		/*
		///////////////////////////////
			//IMAGE
		///////////////////////////////
		*/
	/*
	////////////////////////////////
		//CREATE
	////////////////////////////////
	*/
	/*
	===============================
		SHOW MODAL
	===============================
	*/
		Modal.prototype.showModal = function(){
			this.modal.classList.add("open");
			this.modal.appendChild(this.modalContainer);
			this.body.appendChild(this.modal);
			this.body.style.overflow = "hidden";
			this.carregarUx();
			jsLayout();
			if(this.cbOnload != null){
				var callBack = this.cbOnload;
					callBack();
			}
		}
	/*
	===============================
		//SHOW MODAL
	===============================
	*/
	/*
	===============================
		FECHAR MODAL
	===============================
	*/
		Modal.prototype.fecharModal = function(){
			this.modal.remove();
			this.body.style.overflow = "auto";
			if(this.cbOnClose != null){
				this.cbOnClose();
			}
		}
	/*
	===============================
		//FECHAR MODAL
	===============================
	*/
	/*
	------------------------------
		FEEDBACK
	------------------------------
	*/
		Modal.prototype.feedBack = function(type, code){
			switch(code){
				case 1:
					console.log("Poxa, para usar a função Modal() tem que passar um objeto como parâmetro e os argumentos necessários para ele funcionar.");
					break;
				case 2:
					console.log("Poxa, para que apareça o header no modal tem que passar pelo menos o title.");
					break;
				case 3:
					console.log("Poxa, para que apareça o iframe no modal tem que passar pelo menos a url para exibir.");
					break;
			}
		}
	/*
	------------------------------
		//FEEDBACK
	------------------------------
	*/
	/*
	------------------------------
		CALL
	------------------------------
	*/
		function jsModal(option){
			var modal = new Modal();
			if(option != null){
				modal.carregarOption(option);
			}else{
				modal.feedBack("error", 1)
			}
		}
	/*
	------------------------------
		//CALL
	------------------------------
	*/
	/*
	------------------------------
		ON LOAD
	------------------------------
	*/
		window.onload = function(){
		
		}
	/*
	------------------------------
		//ON LOAD
	------------------------------
	*/
/*
########################################
	//MODAL
########################################
*/