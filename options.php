<header id="header">
  <div class="header_row collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div id="fechaFlutuante" class="clearfix visible-xs">
      <span>X</span>
   </div>

   <div id="tmsearch" class="clearfix">
      <form id="tmsearchbox" method="get" action="">
         <input type="hidden" name="controller" value="search">
         <input type="hidden" name="orderby" value="position">
         <input type="hidden" name="orderway" value="desc">
         <input class="tm_search_query form-control ac_input" type="text" id="tm_search_query" name="search_query" placeholder="Buscar" value="" autocomplete="off">
         <button type="submit" name="tm_submit_search" class="btn btn-default button-search">
         <span>Busca</span>
         </button>
         <span class="btn_show"></span>
      </form>
   </div>

   <div id="layer_cart">
      <div class="clearfix">
         <div class="layer_cart_product col-xs-12 col-md-6">
            <span class="cross" title="Close window"></span>
            <h2>
               <i class="fa fa-ok"></i>
               Produtos adicionados ao carrinho com sucesso.
            </h2>
            <div class="product-image-container layer_cart_img"></div>
            <div class="layer_cart_product_info">
               <span id="layer_cart_product_title" class="product-name"></span>
               <span id="layer_cart_product_attributes"></span>
               <div>
                  <strong class="dark">Qtd</strong>
                  <span id="layer_cart_product_quantity"></span>
               </div>
               <div>
                  <strong class="dark">Total</strong>
                  <span id="layer_cart_product_price"></span>
               </div>
            </div>
         </div>

      </div>
      <div class="crossseling"></div>
   </div>
   <div class="layer_cart_overlay"></div>

 <div id="header-login">
    <div class="header_user_info"><a href="#" onclick="return false;">Sign in</a></div>
      <ul id="header-login-content" class="">
         <li><h3>ÁREA DO CLIENTE</h3></li>
         <li><h2>Quero me Cadastrar</h2></li>
         <li>
            <form action="" method="post" id="header_login_form">
               <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
               <div class="form_content clearfix">
                  <div class="form-group">
                     <input class="is_required validate account_input form-control" data-validate="isEmail" placeholder="E-mail" type="text" id="header-email" name="header-email" value="">
                  </div>
                  <div class="form-group">
                     <span><input class="is_required validate account_input form-control" type="password" placeholder="Password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="" autocomplete="off"></span>
                  </div>          
               </div>
            </form>
         </li>
         <li>
            <p class="submit">
               <button type="button" id="HeaderSubmitLogin" name="HeaderSubmitLogin" class="btn btn-default btn-sm">
               <i class="fa fa-lock left"></i>
               CADASTRAR
               </button>
            </p>           
         </li>

         <li><h2 class="cadastrado">Já Sou Cadastrado</h2></li>
         <li>
            <form action="" method="post" id="header_login_form">
               <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
               <div class="form_content clearfix">
                  <div class="form-group">
                     <input class="is_required validate account_input form-control" data-validate="isEmail" placeholder="E-mail" type="text" id="header-email" name="header-email" value="">
                  </div>
                  <div class="form-group">
                     <span><input class="is_required validate account_input form-control" type="password" placeholder="Password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="" autocomplete="off"></span>
                  </div>
                  
               </div>
            </form>
         </li>

         <li>
            <p class="create_p p_entrar">
               <a href="#" class="create">ENTRAR</a>
            </p>
          
         </li>

         <li>
            <a href="#" class="esquecisenha">Esqueci minha senha</a>
          
         </li>

          <li>
            <div class="clearfix">
               <a class="btn btn-default btn-sm btn-login-facebook" href="#" title="Login with Your Facebook Account">
               Facebook Login
               </a>              
            </div>
         </li>

      </ul>
  </div>


   <div class="cart_cont clearfix">
      <div class="shopping_cart">
         <a href="#" title="View my shopping cart" rel="nofollow">
         <b>Carrinho</b>
         <span class="ajax_cart_quantity unvisible">2</span>
         <span class="ajax_cart_product_txt unvisible">Produto</span>
         <span class="ajax_cart_product_txt_s unvisible">Produtos</span>
         <span class="ajax_cart_total unvisible">
         </span>
         <span class="ajax_cart_no_product">(empty)</span>
         </a>
         <span class="cart_btn"></span>
         <div class="cart_block block">
            <div class="block_content">
               <div class="cart_block_list">
                  <div class="col-lg-12">
                    <h1 class="container cart_block_no_products text-center">
                       LISTA DE PRODUTOS
                    </h1>
                  </div>
                  <ul class="listaCarrinho">
                      <li>
                        <div class="col-lg-4 col-sm-4">
                            <img src="./images/produtos/produto2.png" alt="Produto">
                        </div>
                        <div class="col-lg-7 col-sm-7">
                            <h1>Matsuda Top Line Cria</h1><br>
                        </div>
                        <div class="col-lg-1 col-sm-1">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                      </li>
                      <li>
                        <div class="col-lg-4 col-sm-4">
                            <img src="./images/produtos/produto2.png" alt="Produto">
                        </div>
                        <div class="col-lg-7 col-sm-7">
                            <h1>Matsuda Top Line Cria</h1><br>
                        </div>
                        <div class="col-lg-1 col-sm-1">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                      </li>
                       <li>
                        <div class="col-lg-4 col-sm-4">
                            <img src="./images/produtos/produto2.png" alt="Produto">
                        </div>
                        <div class="col-lg-7 col-sm-7">
                            <h1>Matsuda Top Line Cria</h1><br>
                        </div>
                        <div class="col-lg-1 col-sm-1">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                      </li>
                  </ul>
               </div>
               <div class="cart_block_finalizar oswald">
                  <button>FINALIZAR ORÇAMENTO</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   
  </div>
</header>