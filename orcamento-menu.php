<div class="row col-lg-12 col-xs-12 col-sm-12 orcamento-menu ">
	<div class="col-lg-12 col-xs-12 col-sm-12 abas">
		<ul id="myTabs" role="tablist">	
			<li role="presentation" class="active col-lg-3 col-sm-3 col-xs-12 text-center" class="col-lg-10 center ">
				<a href="#lista" aria-controls="lista" role="tab" data-toggle="tab" class="col-lg-10 col-xs-12  center text-center">
					<div class="orc-menu--list">
						<h1>Lista de produtos</h1>
						<div class="etapas">
							<div class="etapas-icon">
								<span>1</span>
							</div>
						</div>
					</div>
				</a>
			</li>
			<li role="presentation" class=" col-lg-3 col-sm-3 col-xs-12">
				<a href="#informacoes" aria-controls="garantia" role="tab" data-toggle="tab" class="col-lg-10 col-xs-12 center text-center">
					<div class="orc-menu--list">
						<h1>Suas Informações</h1>
						<div class="etapas">
							<div class="etapas-icon">
								<span>2</span>
							</div>
						</div>
					</div>
				</a>
			</li>
			<li role="presentation" class=" col-lg-3 col-sm-3 col-xs-12">
				<a href="#confirmar" aria-controls="desc-abas" role="tab" data-toggle="tab" class="col-lg-10 col-xs-12 center text-center">
					<div class="orc-menu--list">
						<h1>Confirmar Dados</h1>
						<div class="etapas">
							<div class="etapas-icon">
								<span>3</span>
							</div>
						</div>
					</div>
				</a>
			</li>
			<li role="presentation" class=" col-lg-3 col-sm-3 col-xs-12 text-center">
				<a href="#finalizar" aria-controls="desc-abas" role="tab" data-toggle="tab" class="col-lg-10 col-xs-12 center text-center">
					<div class="orc-menu--list">
						<h1>Finalizar Orçamento</h1>
						<div class="etapas">
							<div class="etapas-icon">
								<span>4</span>
							</div>
						</div>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>