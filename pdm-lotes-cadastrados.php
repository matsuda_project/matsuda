<?php include('topo-login.php'); ?>
	<div class="row col-lg-12 col-md-12 col-xs-12 pdm-inicial">
		<div class="container center">
			<div class="row pdm-inicial--container">
				<div class="row pdm-programa--container text-center">
					<div class="lotes lotes-cadastrados">
						<?php include('pdm-section.php'); ?>
						<div class="row col-lg-12 col-xs-12 col-md-12 lote-selected">
							<div class="lote-selected--container oswald">
								<div class="row fazenda-name ">
									<h1>Fazenda 3 Ilhas</h1>
								</div>
								<div class="row fazenda-local">
									<h2>Presidente Bernardes - SP</h2>
								</div>
								<div class="row inserir-lote">
									<a href="" id="btnCadLotes">INSERIR MAIS LOTES</a>
								</div>
							</div>
						</div>
						<div class="row col-lg-12 col-xs-12 lotes-cadastrados--container">
							<div class="col-lg-8 center">
								<div class="lotes-block col-lg-8">
									<div class="lotes-block--title">
										<h1>Lote A</h1>
									</div>
									<div class="lotes-block--container text-center oswald">
										<div class="block-text ">
											<p>Gado de Corte, a pasto, com 50 animais, início aos 7 meses com média de peso/animal de 105 Kg</p>
										</div>
										<div class="block-begin">
											<p>Início no programa em 19/08/2016</p>
										</div>
									</div>
								</div>
								<div class="lotes-block--options col-lg-4 oswald">
									<div class="block-link text-center ">
										<a href="#">ACESSAR LOTE</a>
									</div>
									<div class="block-select">
										<div class="custom_select">
								          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
								          	</div>
								          	<div class="custom_select_icon right">
								              	<i class="fa fa-caret-down"></i>
								         	</div>
								          	<ul class="custom_select_options">
								              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
								              	<li><a href="#" class="text-center"><i>Produtos Utilizados</i></a></li>
								              	<li><a href="#" class="text-center"><i>Editar Lote</i></a></li>
								              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
								          	</ul>
								        </div>
									</div>
								</div>
							</div>
						</div>
						<div class="row col-lg-12 col-xs-12 lotes-cadastrados--container">
							<div class="col-lg-8 center">
								<div class="lotes-block col-lg-8">
									<div class="lotes-block--title">
										<h1>Lote B</h1>
									</div>
									<div class="lotes-block--container text-center oswald">
										<div class="block-text ">
											<p>Gado de Corte, a pasto, com 50 animais, início aos 7 meses com média de peso/animal de 105 Kg</p>
										</div>
										<div class="block-begin">
											<p>Início no programa em 19/08/2016</p>
										</div>
									</div>
								</div>
								<div class="lotes-block--options col-lg-4 oswald">
									<div class="block-link text-center ">
										<a href="#">ACESSAR LOTE</a>
									</div>
									<div class="block-select">
										<div class="custom_select">
								          	<div class="custom_select_selected text-center"><a><i>Selecione</i></a>
								          	</div>
								          	<div class="custom_select_icon right">
								              	<i class="fa fa-caret-down"></i>
								         	</div>
								          	<ul class="custom_select_options">
								              	<li><a href="#" class="text-center"><i>Editar</i></a></li>
								              	<li><a href="#" class="text-center"><i>Produtos Utilizados</i></a></li>
								              	<li><a href="#" class="text-center"><i>Editar Lote</i></a></li>
								              	<li><a href="#" class="text-center"><i>Excluir</i></a></li>
								          	</ul>
								        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

















<?php include('modalCadLotes.php'); ?>
<?php include('rodape-login.php'); ?>