<div class="row suplementos-container">
    <div class="container">
        <div class="col-lg-2 col-sm-4 col-xs-12 col-md-2 left">
            <div class="suplementos-block text-center container b1">
                <div class="img-vaca1 block-img-vaca1 block-img center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title">
                        <h1>GADO DE <span>CORTE</span></h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Produtos para cria, recria e engorda</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">CONHEÇA</a>
                </div>
            </div>
        </div>
        <div class="col-lg-2  col-sm-4 col-xs-12 col-md-2  left">
            <div class="suplementos-block text-center container b2">
                <div class="block-img img-vaca2 block-img-vaca2 center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title">
                        <h1>GADO DE <span>LEITE</span></h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Produtos de pronto uso, núcleos e proteicos energéticos</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-2  col-sm-4 col-xs-12 col-md-2 left">
            <div class="suplementos-block text-center container b3">
                <div class="block-img img-cavalo block-img-cavalo center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title">
                        <h1>EQUÍDEOS</h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Linha branca, proteicos energéticos e núcleos</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-4 col-xs-12 col-md-2  left">
            <div class="suplementos-block text-center container b4">
                <div class="block-img img-ovino block-img-ovino center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title">
                        <h1>OVINOS</h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Suplemento Mineral, Núcleo Mineral e Rações Mineralizadas</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-2  col-sm-4 col-xs-12 col-md-2  left">
            <div class="suplementos-block text-center container b5">
                <div class="block-img block-img-caprino img-caprino center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title"> 
                        <h1>CAPRINOS</h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Suplemento Mineral, Núcleo Mineral e Rações Mineralizadas</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-sm-4 col-xs-12 col-md-2  left">
            <div class="suplementos-block text-center container b6">
                <div class="block-img block-img-babulino img-babulino center">
                </div>
                <div class="control-suplementos">
                    <div class="block-title">
                        <h1>BUBALINOS</h1>
                    </div>
                    <div class="block-desc container center">
                        <p>Suplemento Mineral para Bubalinos</p>
                    </div>
                </div>
                <div class="block-link oswald">
                    <a href="">SAIBA MAIS</a>
                </div>
            </div>
        </div>
      
    </div>
</div>