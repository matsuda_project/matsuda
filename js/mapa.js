var map;
 
function initialize() {
    var latlng = new google.maps.LatLng(-22.094313,-51.476361);
 
    var options = {
        zoom: 10,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    map = new google.maps.Map(document.getElementById("mapa"), options);
}
 
initialize();

function carregarPontos() {
 
    $.getJSON('js/pontos.json', function(pontos) {
 
        $.each(pontos, function(index, ponto) {

            var marker = new google.maps.Marker({
			    position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
			    title: "Matsuda",
			    map: map,
			    icon: 'img/marcador.png'
			});

            var infowindow = new google.maps.InfoWindow(), marker;

            var informacoes = ponto.titulo + ponto.endereco + ponto.telefone + ponto.email;
 
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
			    return function() {
			        infowindow.setContent(informacoes);
			        infowindow.open(map, marker);
			    }
			})(marker))
 
        });
 
    });
 
}
 
carregarPontos();
