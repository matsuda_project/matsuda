$(document).ready(function() {
	$(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#Top').fadeIn();
        } else {
            $('#Top').fadeOut();
        }
    });

    $('#Top').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });


	$('#fechaFlutuante').click(function(event){
		$('#mostraMenu').click();
	});

	$('#menuVideoTeca').click(function(event){
		event.preventDefault();		
		$('#MenuVideoTeca').removeClass('moved-left ');
		$('#MenuVideoTeca').addClass('moved-right');		
		return false;

	});

	$('#btnFechaMenu').click(function(event){
		$('#MenuVideoTeca').removeClass('moved-right');		
		$('#MenuVideoTeca').addClass('moved-left ');
	});

	$('#abreSegmentosMenu').click(function(event){
		event.preventDefault();	
		$('#MenuVideoTeca').removeClass('moved-right');		
		$('#MenuVideoTeca').addClass('moved-left ');

		$('#SegmentosMenu').removeClass('moved-left ');
		$('#SegmentosMenu').addClass('moved-right');		
		return false;

	});

	$('#btnFechaMenu2').click(function(event){
		event.preventDefault();
		$('#SegmentosMenu').removeClass('moved-right');		
		$('#SegmentosMenu').addClass('moved-left ');

		$('#MenuVideoTeca').removeClass('moved-left ');
		$('#MenuVideoTeca').addClass('moved-right');
	});

	

	

	var $btns = $('.btnFiltro').click(function(event) {
      event.preventDefault();
	  if (this.id == 'all') {
	    $('#parent > div').fadeIn(450);
	  } else {
	    var $el = $('.' + this.id).fadeIn(450);
	    $('#parent > div').not($el).hide();
	  }
	  $btns.removeClass('active');
	  $(this).addClass('active');
	})

	$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});

	$(".rslides").responsiveSlides({
      auto: true,             // Boolean: Animate automatically, true or false
      speed: 500,            // Integer: Speed of the transition, in milliseconds
      timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
      pager: true,           // Boolean: Show pager, true or false
      nav: false,             // Boolean: Show navigation, true or false
      random: false,          // Boolean: Randomize the order of the slides, true or false
      pause: false           // Boolean: Pause on hover, true or false
    });

    $(".aovivo").stop(true, true).delay(200).fadeOut(0);

    $("#aviso").click(function(){
    	$(".videoDir").stop(true, true).delay(200).fadeOut(0);  
    	$('.aovivo').stop(true, true).delay(200).fadeIn(500);    	  
    });

    $("#voltarAoVivo").click(function(){
    	$(".aovivo").stop(true, true).delay(200).fadeOut(0);
    	$('.videoDir').stop(true, true).delay(200).fadeIn(500);    	
    });

    /*

     $('.slider1').bxSlider({
	    slideWidth: 200,
	    minSlides: 1,
	    maxSlides: 6,
	    slideMargin: 10,
	    pager: false
	  });

     $('.sliderNutricao').bxSlider({
	    slideWidth: 200,
	    minSlides: 1,
	    maxSlides: 4,
	    slideMargin: 8,
	    pager: false
	  });

	  */
});
