<?php

?>
<div class="wide preRodape">
    <div class="container container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="blocoUnidade">
            <p class="unidade">Com sede em Álvares Machado - SP</p>
            <p class="telefone"><img src="img/iconTelefoneRodape.png" alt="Telefone" /><span class="ddd">+55 (18)</span> 3226-2000</p>
            <p class="maisinfos"><a href="#">TELEFONE DE TODAS UNIDADES</a></p>
          </div>
        </div>

        <div class="col-xs-12 col-sm-2 col-md-2">
          <p class="tituloListaRodape"><img src="img/titulorodape.png" alt="" />INSTITUCIONAL</p>
          <ul class="listaRodape">
            <li><a href="#">Quem Somos?</a></li>
            <li><a href="#">Nossas Unidades</a></li>
            <li><a href="#">Onde Comprar</a></li>
            <li><a href="#">Contatos</a></li>
            <li><a href="#">Webmail</a></li>
            <li><a href="#">Intranet</a></li>
            <li><a href="#">Trabalhe Conosco</a></li>
          </ul>
        </div>

       
        
        <div class="col-xs-12 col-sm-2 col-md-2">
          <p class="tituloListaRodape"><img src="img/titulorodape.png" alt="" />SEGMENTOS</p>
          <ul class="listaRodape">
            <li><a href="#">Sementes para Pastagem</a></li>
            <li><a href="#">Nutrição Animal</a></li>
            <li><a href="#">Inoculantes</a></li>
            <li><a href="#">Equipamentos</a></li>
            <li><a href="#">Energia Solar</a></li>
            <li><a href="#">Rações para Peixes</a></li>
            <li><a href="#">Pet</a></li>
            <li><a href="#">Saúde Animal</a></li>
          </ul>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="blocoUnidade">            
            <p class="maisinfos"><a href="#" class="cadastreseRodape">CADASTRE-SE EM NOSSO SITE</a></p>
          </div>
        </div>
        
      </div>
    </div>
  </div>


    <footer class="footerInterno">
      <div class="container container-fluid">   
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4 politica_privacidade">
            <a href="#">Politica de Privacidade</a>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4  hidden-xs">
            <p>&copy; Copyright 2017 Matsuda Com. Ind. Imp. Exp. Ltda</p>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4  hidden-xs">
             <div class="redessociais">
                <ul>
                  <li><a href="#"><img src="img/iconFace.jpg" alt="Facebook"></a></li>
                  <li><a href="#"><img src="img/iconYoutube.png" alt="Youtube"></a></li>
                  <li><a href="#"><img src="img/iconInstagran.jpg" alt="Instagran"></a></li>
                  <li><a href="#"><img src="img/iconLinkedin.jpg" alt="Linkedin"></a></li>
                  <li><a href="#"><img src="img/iconTwitter.jpg" alt="Twitter"></a></li>
                  <li><a href="#"><img src="img/iconGoogle.jpg" alt="Google"></a></li>
                  <li><a href="#"><img src="img/iconPin.jpg" alt="Pin"></a></li>
                </ul>

              </div>
          </div>
        </div>
      </div>
    </footer>

  </div>
  <!-- /site -->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">CADASTRE-SE EM NOSSO SITE</h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
              <form class="form-horizontal">
                <div class="form-group">                  
                  <div class="col-sm-12">
                    <input type="Nome" class="form-control" id="inputNome3" placeholder="Nome">
                  </div>
                </div>
                <div class="form-group">
                  
                  <div class="col-sm-12">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="E-mail">
                  </div>
                </div> 
              </form>
              
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 tenhaacesso">
              <h3>TENHA ACESSO:</h3>
              <p>- Banco de antigos técnicos</p>
              <p>- Videoteca personalizada</p>
              <p>- Cursos OnLine</p>
              <p>- Seus pedidos de orçamento</p>
              <p>- Consulta técnica</p>
            </div>
         </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<a href="#Top" class="backTop" id="Top">
  <img src="img/btnVoltaTopop.png" />
</a>

    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/ResponsiveSlides/responsiveslides.min.js"></script>
    <link href="js/ResponsiveSlides/responsiveslides.css" rel="stylesheet" type="text/css">

    <!-- bxSlider Javascript file -->
    <script src="js/bxslider/jquery.bxslider.min.js"></script>
    <!-- bxSlider CSS file -->
    <link href="js/bxslider/jquery.bxslider.css" rel="stylesheet" />

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/animation.js"></script>
    <script src="js/menulateral.js"></script>
    <script type="text/javascript" src="js/nora.1.0.min.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>
