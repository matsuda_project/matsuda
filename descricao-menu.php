<div class="row col-lg-12 col-xs-12 menu-descricao active" id="menu-level">
	<div class="row menu-descricao--container">
		<div class="container center">
		    <ul class="nav nav-tabs" role="tablist">
		    	<li role="presentation" class="active col-lg-2 col-xs-12 col-sm-4 col-md-2 ">
					<div class="icon-b1 center">
						<a class=" icon-b1-block icon-b1-img center" href="#cria-menu" aria-controls="corte" role="tab" data-toggle="tab">
							<div class="text-center control">
								<div class="col-lg-5 center">
									Bovinos de Corte
								</div>
							</div>
						</a>
					</div>
		    	</li>
		    	<li role="presentation" class="col-lg-2 col-xs-12 col-sm-4 col-md-2">
					<div class="icon-b2 center">
						<a class="icon-b2-block icon-b2-img center" href="#cria-menu" aria-controls="leite" role="tab" data-toggle="tab">
							<div class="text-center control">
								<div class="col-lg-5 center">
									Bovinos de Leite
								</div>
							</div>
						</a>
					</div>
				</li>
				<li role="presentation" class=" col-lg-2 col-xs-12 col-sm-4 col-md-2 ">
					<div class="icon-b3 center">
						<a class="icon-b3-block icon-b3-img center" href="#cria-menu" aria-controls="equideos" role="tab" data-toggle="tab"">
							<div class="text-center control eq">
								<div class="col-lg-5 center">
									Equídeos
								</div>
							</div>
						</a>
					</div>
				</li>
				<li role="presentation" class=" col-lg-2 col-xs-12 col-sm-4 col-md-2 ">
					<div class="icon-b4 center">
						<a class="icon-b4-block icon-b4-img center" href="#cria-menu" aria-controls="ovinos" role="tab" data-toggle="tab">
							<div class="text-center control">
								<div class="col-lg-5 center">
									Ovinos
								</div>
							</div>
						</a>
					</div>
				</li>
				<li role="presentation" class=" col-lg-2 col-xs-12 col-sm-4 col-md-2 ">
					<div class="icon-b5 center">
						<a class="icon-b5-block icon-b5-img center" href="#cria-menu" aria-controls="caprinos" role="tab" data-toggle="tab">
							<div class="text-center control">
								<div class="col-lg-5 center">
									Caprinos
								</div>
							</div>
						</a>
					</div>
				</li>
				<li role="presentation" class=" col-lg-2 col-xs-12 col-sm-4 col-md-2 ">
					<div class="icon-b6">
						<a class="icon-b6-block icon-b6-img center" href="#cria-menu" aria-controls="bubalinos" role="tab" data-toggle="tab">
							<div class="text-center control">
								<div class="col-lg-5 center">
									Bubalinos
								</div>
							</div>
						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>	
</div>	
<div class="row col-lg-12 col-xs-12 menu-sub" id="cria-menu">
	<div class="container center">
		<div class="row menu-sub--container">
			<ul class="nav nav-tabs text-center" role="tablist">
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Creep Feeding</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Cria</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Recria</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Engorda</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Confinamento</span>	
					</a>
				</li>
				<li role="presentation" class="prot" >
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Proteico Energético para semi confinamento</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Mistura com sal branco</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#prod-desc-menu" aria-controls="profile" role="tab" data-toggle="tab">
						<span>Linha Vitta</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="row col-lg-12 col-xs-12  menu-sub--row center text-center">
			<ul>
				<li><a href="">Todas as Categorias</a></li>
				<li><a href="">Bovinos de Corte</a></li>
				<li><a href="#menu-level" aria-controls="return" role="tab" data-toggle="tab"><i></i>Retornar</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="row col-lg-12 col-xs-12 menu-sub" id="prod-desc-menu">
	<div class="container center">
		<div class="row menu-sub--container sub">
			<div class="text-center">
				<ul>
					<li><a href=""><span>Linha Branca</span></a></li>
					<li><a href=""><span>Proteínado das Águas</span></a></li>
					<li><a href=""><span>Proteinado de Seca</span></a></li>
					<li><a href=""><span>Ureado</span></a></li>
					<li><a href=""><span>Água Dura e Salobra</span></a></li>
				</ul>
			</div>
		</div>
		<div class="row col-lg-12 col-xs-12  menu-sub--row center text-center">
			<ul>
				<li><a href="">Bovinos de Corte</a></li>
				<li><a href="">Cria</a></li>
				<li><a href="#cria-menu" aria-controls="return" role="tab" data-toggle="tab"><i></i>Retornar</a></li>
			</ul>
		</div>
	</div>
</div>