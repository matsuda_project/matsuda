
<div class="wide preRodape hidden-xs">
    <div class="container container-fluid">
      <div class="row seven-cols col-lg-12">
        <div class="col-lg-3 col-md-1">
          <p class="tituloListaRodape">DESEMPENHO MÁXIMO</p>
          <ul class="listaRodape">
            <li><a href="#">Mais Carne Pasto</a></li>
            <li><a href="#">Mais Carne Confinamento</a></li>
            <li><a href="#">Mais Leite</a></li>
            <li><a href="#">Mais Bezerros</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-1">
          <p class="tituloListaRodape">BOVINOS DE CORTE</p>
          <ul class="listaRodape">
            <li><a href="#">Creep Feeding</a></li>
            <li><a href="#">Cria</a></li>
            <li><a href="#">Recria</a></li>            
            <li><a href="#">Engorda</a></li>            
            <li><a href="#">Confinamento</a></li>            
            <li><a href="#">Mistura de Sal Branco</a></li>            
            <li><a href="#">Linha Vitta</a></li>            
          </ul>
        </div>
        
        <div class="col-lg-3 col-md-1">
          <p class="tituloListaRodape">BOVINOS DE CORTE</p>
          <ul class="listaRodape">
            <li><a href="#">Pronto para Uso</a></li>
            <li><a href="#">Núcleos</a></li>
            <li><a href="#">Linha Lac</a></li>
            <li><a href="#">Protéicos Energéticos</a></li>
            <li><a href="#">Rações Mineralizadas</a></li>
            <li><a href="#">Bezerros Lactentes</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-1">
          <p class="tituloListaRodape">EQUÍDEOS</p>
          <ul class="listaRodape">
            <li><a href="#">Linha Branca</a></li>
            <li><a href="#">Protéicos Energéticos</a></li>
            <li><a href="#">Núcleo</a></li>            
          </ul>

           <div class="col-lg-2 col-md-1">
            <ul class="listaRodape listaRodapeUpper">
              <li><a href="#">OVINOS</a></li>
              <li><a href="#">CAPRINOS</a></li>
              <li><a href="#">BUBALINOS</a></li>           
            </ul>
          </div>
        </div>

        <div class="col-lg-3 col-md-1">
          <ul class="listaRodape listaRodapeUpper">
            <li><a href="#">VÍDEOS</a></li>
            <li><a href="#">ARTIGOS TÉCNICOS</a></li>
            <li><a href="#">QUERO COMPRAR</a></li>
            <li><a href="#">CADASTRE-SE</a></li>
            <li><a href="#">FALE COM UM TÉCNICO</a></li>
            <li><a href="#">SAC</a></li>            
          </ul>
        </div>
        
      </div>
    </div>
  </div>
    <div class="col-lg-12 footer-whiteblock">
        <div class="container center">
            <div class="col-lg-8 center">
                <div class="row nav-links text-center">
                    <nav>
                        <ul>
                            <li><a href="#" class="colorGrupo">GRUPO MATSUDA</a></li>
                            <li><a href="">Sementes</a></li>
                            <li><a href="">Nutrição Animal</a></li>
                            <li><a href="">Inoculantes</a></li>
                            <li><a href="">Equipamentos</a></li>
                            <li><a href="">Energia Solar</a></li>
                            <li><a href="">Peixes</a></li>
                            <li><a href="">Pets</a></li>
                            <li><a href="">Saúde Animal</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-3 container center">
                <div class="redessociais col-lg-10 center">
                    <div class="redessociais-title text-center">
                        <p>Redes Sociais</p>
                    </div>
                    <ul>
                        <li><a href="#"><img src="img/iconFace.jpg" alt="Facebook"></a></li>
                        <li><a href="#"><img src="img/iconYoutube.png" alt="Youtube"></a></li>
                        <li><a href="#"><img src="img/iconInstagran.jpg" alt="Instagran"></a></li>
                        <li><a href="#"><img src="img/iconLinkedin.jpg" alt="Linkedin"></a></li>
                        <li><a href="#"><img src="img/iconTwitter.jpg" alt="Twitter"></a></li>
                        <li><a href="#"><img src="img/iconGoogle.jpg" alt="Google"></a></li>
                        <li><a href="#"><img src="img/iconPin.jpg" alt="Pin"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer>
      <div class="container container-fluid">   
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <p>Copyright &copy; 2015 Matsuda - Todos os direitos reservados.</p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 politicaprivacidade  hidden-xs">
            <p><a href="#">Política de Privacidade</a></p>
          </div>
        </div>
      </div>
    </footer>
  </div>

<a href="#Top" class="backTop" id="Top">
  <img src="img/btnVoltaTopop.png" />
</a>


    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/ResponsiveSlides/responsiveslides.min.js"></script>
    <link href="js/ResponsiveSlides/responsiveslides.css" rel="stylesheet" type="text/css">

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/animation.js"></script>
    <script src="js/menulateral.js"></script>
    <script type="text/javascript" src="js/nora.1.0.min.js"></script>



    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>
