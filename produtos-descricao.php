<?php include('topo-pages.php'); ?>
	<?php include('options.php'); ?>
	
	<?php include('descricao-menu.php'); ?>

	<div class="row col-lg-12 col-xs-12 produtos-descricao">
		<div class="container center">
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container control">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-xs-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center ">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque2.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-sm-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 co-xs-10 center">
								<h1>Matsuda Top Line Cria</h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-md-2 col-xs-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Top Line Primípara</h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-sm-2 col-xs-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Fós Prime</h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-sm-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Top Line Cria </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Top Line Primípara</h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-sm-2 col-xs-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Fós Prime</h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center oswald">
						<div class="produtos-block--tipo left">
							<span>Engorda</span>
						</div>
						<div class="produtos-block--tipo left">
							<span>Reria</span>
						</div>
						<div class="produtos-block--tipo left">
							<span>Cria</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-sm-2 col-xs-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-sm-2 col-md-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-lg-3 col-xs-12 col-sm-4 col-md-3 container">
				<div class="row produtos-block">
					<div class="row col-lg-10 col-md-10 col-sm-10 left">
						<div class="produtos-block--img text-center">
							<img src="./images/produto-destaque.png">
						</div>
					</div>
					<div class="row col-lg-2 col-xs-2 col-md-2 col-sm-2 right">  
						<div class="row produtos-block--options right">
							<div class="icon-block">
								<button><img src="./images/icons/carrinho.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/troca.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/like.png"></button>
							</div>
							<div class="icon-block">
								<button><img src="./images/icons/mais.png"></button>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12">
						<div class="produtos-block--title text-center">
							<div class="col-lg-9 col-xs-10 center">
								<h1>Matsuda Phós Verão Acabamento </h1>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 col-xs-12 text-center">
						<div class="produtos-block--tipo oswald">
							<span>Engorda</span>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>








<?php include('rodapehome.php'); ?>