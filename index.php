<?php
	include('topohome.php');
?>

    <div class="col-lg-12 col-xs-12 suplementos">
        <div class="container center">
            <?php include('desempenho-maximo-menu.php'); ?>
        </div>
    </div>

    <div class="wide cadastreSeMob cadastreSe">
      <div class="container container-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <h2>CADASTRE-SE EM NOSSO SITE</h2>
          </div>
          <div class="container center">
              <div class="col-lg-10 col-xs-12 center">
                  <div class="container col-lg-7 col-xs-12 col-sm-6 col-md-6 control">
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label for="inputNome" class="col-sm-2 control-label">Nome</label>
                          <div class="container col-lg-10 col-sm-10">
                            <input type="Nome" class="form-control" id="inputNome3" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>
                          <div class="container col-lg-10 col-sm-10">
                            <input type="email" class="form-control" id="inputEmail3" >
                          </div>
                        </div>     
                        <div class="form-group">
                          <div class="col-lg-10 col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">CADASTRAR</button>
                          </div>
                        </div>
                      </form>
                  </div>
                  <div class="container col-lg-5 col-xs-12 col-sm-6 col-md-6 control-acesso mobile-hide">
                    <h3>TENHA ACESSO:</h3>
                    <p>- Videoteca personalizada</p>
                    <p>- Programa Desempenho Máximo</p>
                    <p>- Seus pedidos de orçamento</p>
                    <p>- Consulta técnica</p>
                  </div>   
                
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container container-fluid videos-destaque">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 tituloHome">
            <h2>VÍDEOS EM DESTAQUE</h2>
            <p>nutrição animal</p>
            <a href="#"><img src="img/setaBaixo.png"></a>
        </div>
      </div>
      <div class="row emdestaque">
            <div class="text-right">
                <div class="enter-galery oswald">
                    <a href="">ACESSE A GALERIA</a>
                </div>
            </div>
            <div class="emdestaqueCarousel">
                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                    <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>
                </div>
                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                    <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>          
                </div>
                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                    <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>          
                </div>
                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                    <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>          
                </div>

                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                     <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>          
                </div>

                <div class="colVideo Seg_sementes container">
                    <div class="video">
                      <img src="img/emdestaque.png" class="imgDestaque" />
                      <div class="hoverTitle text-center col-lg-8 col-sm-8  col-xs-8  center">
                         <h1>Faz. Santo Antônio</h1>
                         <img src="./img/iconRun.png">
                      </div>
                      <ul class="likeEmdestaque col-lg-4 col-sm-4">
                        <li><a href="#"><img src="img/iconLikeVideo.png" alt="Adicionar aos Favoritos"></a></li>
                        <li><a href="#"><img src="img/iconFaceVideo.png" alt="Compartilhar"></a></li>
                        <li><a href="#"><img src="img/youtube-novo.png" alt="Compartilhar"></a></li>
                      </ul>
                      <div class="categoria">NUTRIÇÃO ANIMAL</div>
                    </div>
                     <div class="col-lg-10">
                        <p class="titulo">Fós Reprodução é utilizado na Fazenda Santo Antônio</p>            
                    </div>           
                </div>       
            </div>
      </div>



      <div class="col-lg-12 home-services">
          <div class="container center">
              <div class="services-block">
                  <div class="col-lg-4 col-md-6 col-sm-6 container control-transparence control-transparence-user">
                      <div class="row services-block-container">
                          <div class="services-block--left col-lg-7 col-md-7 col-sm-7 col-xs-6 left">
                              <div class="left-content">
                                  <div class="content-tile">
                                      <h1>FALE COM UM <span>TÉCNICO</span></h1>
                                  </div>
                                  <div class="content-text">
                                      <p>Entre em contato com nosso departamento técnico de Nutrição Animal</p>
                                  </div>
                              </div>
                          </div>
                          <div class="services-block--right col-md-5 col-lg-5 col-sm-5 col-xs-5 right mobile-hide">
                              <div class="icon-user right-image-user right">
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 center">
                          <div class="services-block--link text-center oswald">
                              <a href="">ACESSAR</a>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-sm-6 col-md-6 container control-transparence control-transparence-archive">
                      <div class="row services-block-container">
                          <div class="services-block--left col-lg-7 col-md-7 col-sm-7 col-xs-6 left">
                              <div class="left-content">
                                  <div class="content-tile">
                                      <h1>INFORMATIVO <span>ARTIGOS TÉCNICOS</span></h1>
                                  </div>
                                  <div class="content-text">
                                      <p>Acesse nosso banco de artigos técnicos sobre diversos temas pertinentes à nutrição animal de grandes animais</p>
                                  </div>
                              </div>
                          </div>
                          <div class="services-block--right col-lg-5 col-md-5 col-sm-5 col-xs-5 right mobile-hide">
                              <div class="icon-archive right-image-archive center">
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 center">
                          <div class="services-block--link text-center oswald">
                              <a href="">CLIQUE AQUI</a> 
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-6 container control-transparence control-transparence-price">
                      <div class="row services-block-container">
                          <div class="services-block--left col-lg-8 col-md-7 col-sm-7 col-xs-6 left">
                              <div class="left-content">
                                  <div class="content-tile">
                                      <h1>PERTO DE VOCÊ <span>QUERO COMPRAR</span></h1>
                                  </div>
                                  <div class="content-text">
                                      <p>Entre em contato com nosso departamento técnico de Nutrição Animal</p>
                                  </div>
                              </div>
                          </div>
                          <div class="services-block--right col-lg-4 col-md-5 col-xs-4 right mobile-hide">
                              <div class="icon-price right-image-price right">
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 center">
                          <div class="services-block--link text-center oswald">
                              <a href="">ONDE ENCONTRO?</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>

 <?php
	include('rodapehome.php');
?>
