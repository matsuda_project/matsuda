<?php include('topo-pages.php'); ?>
	<div class="col-lg-12 col-xs-12 cadastro-pf">
		<div class="container center">
			<div class="cadastro-pf--block">
				<div class="col-lg-6 col-xs-12 left block-control container">
					<div class="block-title">
						<p>Dados pessoais</p>
					</div>
					<div class="block-form block-dados-pessoais">
						<form>
							<div class="form-field">
								<div class="row user-identity">
									<div class="left col-lg-4 col-xs-6">
										<div class="left">
											<input type="radio" aria-label="" name="tipo-user" value="pf" class="radio">
										</div>
										<div class="left">
											<p>Pessoa Física</p>
										</div>
									</div>
									<div class="left col-lg-4 col-xs-6">
										<div class="left">
											<input type="radio" aria-label="" name="tipo-user" value="pj" class="radio">
										</div>
										<div class="left">
											<p>Pessoa Jurídica</p>
										</div>
									</div>
								</div>
								<div class="row col-lg-12 col-xs-12 input-group">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    <input class="mdl-textfield__input"  type="text" id="sample3" required>
									    <label class="mdl-textfield__label" for="sample3">CPF:</label>
								    </div>
								</div>
								<div class="row user-identity">
									<div class="left col-lg-4 col-xs-6">
										<div class="left">
											<input type="radio" aria-label="" name="sexo" value="m" class="radio">
										</div>
										<div class="left">
											<p>Masculino</p>
										</div>
									</div>
									<div class="left col-lg-4 col-xs-6">
										<div class="left">
											<input type="radio" aria-label="Pessoa Fisica" name="sexo" value="f" class="radio">
										</div>
										<div class="left">
											<p>Feminino</p>
										</div>
									</div>
								</div>
								<div class="row col-lg-12 col-xs-12 input-group">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    <input class="mdl-textfield__input"  type="text" id="sample3" required>
									    <label class="mdl-textfield__label" for="sample3">Nome Completo:</label>
								    </div>
								</div>
								<div class="row col-lg-12 col-xs-12 input-group">
									<div class="row">
										<label>Data de Nascimento:</label>
									</div>
									<div class="row">
										<div class="left col-xs-4">
											<select>
							                  	<option>Dia</option>
							                </select>
										</div>
						                <div class="left col-xs-4">
											<select>
							                  	<option>Mês</option>
							                </select>
										</div>
						                <div class="left col-xs-4">
											<select>
							                  	<option>Ano</option>
							                </select>
										</div>
									</div>
								</div>
								<div class="password">
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Criar Senha:</label>
									    </div>
									</div>
									<div class="row col-lg-12 col-xs-12 input-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										    <input class="mdl-textfield__input"  type="text" id="sample3" required>
										    <label class="mdl-textfield__label" for="sample3">Confirmar Senha:</label>
									    </div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12 left block-control container">
					<div class="block-title">
						<p>Dados de Contato</p>
					</div>
					<div class="block-form block-dados-contato">
						<form>
							<div class="form-field">
								<div class="row col-lg-12 col-xs-12 input-group">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
									    <input class="mdl-textfield__input"  type="email" id="sample3"required>
									    <label class="mdl-textfield__label" for="sample3">E-mail:</label>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
									    <input class="mdl-textfield__input"  type="text" id="sample3" required>
									    <label class="mdl-textfield__label" for="sample3">Confirmar e-mail:</label>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
									    <input class="mdl-textfield__input"  type="text" id="sample3" required>
									    <label class="mdl-textfield__label" for="sample3">Telefone Residencial:</label>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
									    <input class="mdl-textfield__input"  type="text" id="sample3" required>
									    <label class="mdl-textfield__label" for="sample3">Telefone Celular:</label>
								    </div>
								    <div class="col-lg-9 col-xs-8 left">
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Rua / Av:</label>
								    	</div>
								    </div>
								    <div class="col-lg-3 col-xs-4 left control container">
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Número:</label>
								    	</div>
								    </div>
								    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
								    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
								    	<label class="mdl-textfield__label" for="sample3">Complemento:</label>
							    	</div>
							    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
								    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
								    	<label class="mdl-textfield__label" for="sample3">Bairro:</label>
							    	</div>
							    	<div class="col-lg-9 col-xs-8 left">
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
									    	<label class="mdl-textfield__label" for="sample3">Cidade:</label>
								    	</div>
							    	</div>
							    	<div class="col-lg-3 col-xs-4 control container">
								    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									    	<div class="left col-lg-12">
												<select>
								                  	<option>Estado</option>
								                </select>
											</div>
								    	</div>
							    	</div>
							    	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-xs-12">
								    	<input class="mdl-textfield__input"  type="text" id="sample3" required>
								    	<label class="mdl-textfield__label" for="sample3">País:</label>
							    	</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12 left block-control container">
					<div class="block-title">
						<p>Gostariamos de saber</p>
					</div>
					<div class="block-form block-dados-newsletter">
						<form>
							<div class="form-field">
								<div class="row col-lg-12 col-xs-12 input-group">
									<div class="col-lg-4 col-xs-12">
										<label>É cliente Matsuda?</label>
									</div>
									<div class="col-lg-4 col-xs-12 input-group--cliente">
										<div class="col-lg-6 col-xs-6">
											<div class="left">
												<input type="radio" name="cliente" value="sim" class="radio">
											</div>
											<div class="left">
												<p>Sim</p>
											</div>
										</div>
										<div class="col-lg-6 col-xs-6">
											<div class="left">
												<input type="radio" name="cliente" value="nao" class="radio">
											</div>
											<div class="left">
												<p>Não</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row col-lg-12 col-xs-12 input-group info">
									<div class="col-lg-12 col-xs-12">
										<label>Quero receber e-mail’s sobre produtos dos segmentos:</label>
									</div>
									<div class="col-lg-6 col-xs-12 left">
										<div class="row col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Nutrição Animal">
											</div>
											<div class="left">
												<p>Nutrição Animal</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Sementes para Pastagem">
											</div>
											<div class="left">
												<p>Sementes para Pastagem</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Equipamentos Agrícolas">
											</div>
											<div class="left">
												<p>Equipamentos Agrícolas</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Inoculantes para Silagem">
											</div>
											<div class="left">
												<p>Inoculantes para Silagem</p>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-xs-12 left">
										<div class="row col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Saúde Animal">
											</div>
											<div class="left">
												<p>Saúde Animal</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Rações para Peixes">
											</div>
											<div class="left">
												<p>Rações para Peixes</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Energia Solar">
											</div>
											<div class="left">
												<p>Energia Solar</p>
											</div>
										</div>
										<div class="col-lg-12 col-xs-12 checkbox-control">
											<div class="left">
												<input type="checkbox" name="receberemail" id="" class="checkbox" value="Alimentos Pet">
											</div>
											<div class="left">
												<p>Alimentos Pet</p>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-xs-12 newsletter-emails">
										<div class="row ">
											<label>Aceito receber promoções e e-mails institucionais</label>
										</div>
										<div class="col-lg-2 col-xs-6">
											<div class="left">
												<input type="radio" name="newsletter" value="sim" class="radio">
											</div>
											<div class="left">
												<p>Sim</p>
											</div>
										</div>
										<div class="col-lg-2 col-xs-6">
											<div class="left">
												<input type="radio" name="newsletter" value="nao" class="radio">
											</div>
											<div class="left">
												<p>Não</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 row">
					<div class="row btn-confirmar">
						<div class="col-lg-4 center">
							<div class="text-center oswald">
								<button type="submit">CONFIRMAR CADASTRO</button>
								<img src="./images/cadastro-buttom.png">
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>








<?php include('rodapehome.php'); ?>