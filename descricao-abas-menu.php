<div class="row col-lg-12 col-xs-12 info-menu ">
	<div class="container center">
		<div class="col-lg-12 col-xs-12">
			<ul id="myTabs" role="tablist">	
				<li role="presentation" class="active col-lg-3 col-xs-12 text-center">
					<a href="#clientes" aria-controls="clientes" role="tab" data-toggle="tab">
						<div class="info-menu--list">
							<h1>CLIENTES QUE USAM</h1>
						</div>
					</a>
				</li>
				<li role="presentation" class=" col-lg-3 col-xs-12 text-center">
					<a href="#garantia" aria-controls="garantia" role="tab" data-toggle="tab">
						<div class="info-menu--list">
							<h1>NÍVEIS DE GARANTIA</h1>
						</div>
					</a>
				</li>
				<li role="presentation" class=" col-lg-3 col-xs-12 text-center">
					<a href="#usar" aria-controls="desc-abas" role="tab" data-toggle="tab">
						<div class="info-menu--list">
							<h1>MODO DE USAR</h1>
						</div>
					</a>
				</li>
				<li role="presentation" class=" col-lg-3 col-xs-12 text-center">
					<a href="#relacionados" aria-controls="desc-abas" role="tab" data-toggle="tab">
						<div class="info-menu--list">
							<h1>PRODUTOS RELACIONADOS</h1>
						</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>