<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/nora1.0.min.css">

        <link href="css/bootstrap.icon-large.min.css" rel="stylesheet">
 
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
<body>
    <div class="row col-lg-12 col-xs-12 page-impressao">
        <div class="header">
            <div class="container center">
                <div class="header-logo text-center">
                    <img src="./images/logo-imp.png">
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container center">
                <div class="row pedido-date text-right">
                    <p>Data: 05/08/2016</p>
                </div>
                <div class="row section-container">
                    <div class="text-center">
                        <div class="row pedido-status">
                            <p>Orçamento <span>0001/16</span> enviado com sucesso!</p>
                        </div>
                        <div class="row pedido-info">
                            <p>Em breve a empresa que representa a Matsuda em sua região entrará em contato</p>
                        </div>
                        <div class="row pedido-info--cliente">
                            <p>Lembramos que em sua <a href="">área de cliente</a> encontra-se o <a href="">histórico</a> com todos os seus pedidos de orçamento.</p>
                        </div>
                        <div class="row pedido-info--site">
                            Acesse <a href="">www.matsuda.com.br</a> para acessar
                        </div>
                    </div>
                    <div class="row pedidos-list--imp">
                        <div class="row finalizar-list--produtos">
                            <div class="col-lg-10 col-xs-12 center">
                                <div class="row list-container">
                                    <div class="col-lg-7 col-xs-12 center">
                                        <div class="row list-container--block">
                                            <div class="list-container--img col-lg-2 col-xs-3">
                                                <img src="./images/produtos/produto2.png">  
                                            </div>
                                            <div class="list-container--qtd oswald col-lg-4 col-xs-4 text-center">
                                                <p>10 sacos</p>
                                            </div>
                                            <div class="list-container--block col-lg-6 col-xs-5">
                                                <div class="block-name">
                                                    <p>Matsuda Top Line Cria </p>
                                                </div>
                                                <div class="block-resume">
                                                    <p>Embalagens de 25 kg</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row list-container">
                                    <div class="col-lg-7 col-xs-12 center">
                                        <div class="row list-container--block">
                                            <div class="list-container--img col-lg-2 col-xs-3">
                                                <img src="./images/produtos/produto2.png">  
                                            </div>
                                            <div class="list-container--qtd oswald col-lg-4 col-xs-4 text-center">
                                                <p>10 sacos</p>
                                            </div>
                                            <div class="list-container--block col-lg-6 col-xs-5">
                                                <div class="block-name">
                                                    <p>Matsuda Top Line Cria </p>
                                                </div>
                                                <div class="block-resume">
                                                    <p>Embalagens de 25 kg</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row list-container">
                                    <div class="col-lg-7 col-xs-12 center">
                                        <div class="row list-container--block">
                                            <div class="list-container--img col-lg-2 col-xs-3">
                                                <img src="./images/produtos/produto2.png">  
                                            </div>
                                            <div class="list-container--qtd oswald col-lg-4 col-xs-4 text-center">
                                                <p>10 sacos</p>
                                            </div>
                                            <div class="list-container--block col-lg-6 col-xs-5">
                                                <div class="block-name">
                                                    <p>Matsuda Top Line Cria </p>
                                                </div>
                                                <div class="block-resume">
                                                    <p>Embalagens de 25 kg</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-lg-12 col-xs-12 footer">
            <div class="footer-title text-center">
                <p><i>NOSSOS TELEFONES</i></p>
            </div>
            <div class="container center">
                <div class="row unidades-list">
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Álvares Machado - SP (Matriz)</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (18) 3226-2000</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Cuiabá - MT</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (65) 2121-0001</p>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Imperatriz - MA</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (99) 3529-0800</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Curitiba - PR</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (41) 3565-1021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Álvares Machado - SP (Equipamentos)</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (65) 2121-0001</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Goianira - GO</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (62) 3297-5000</p>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Laboratório</p>
                            </div>
                            <div class="unidade-local">
                                <p>Vet&Cia / Jacareí - SP</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (12) 3962-7343</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Centro de Distribuição</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Campo Grande - MS</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (67) 3354-1930</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda São Sebastião do Paraíso - MT</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (65) 2121-0001</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Unidade Fabril</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Vitória da Conquista - BA </p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (77) 3424-2460</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Centro de Distribuição</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Ji-Paraná - RO</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (00) 0000-0000</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 left">
                        <div class="list-container container">
                            <div class="unidade-title">
                                <p>Centro de Distribuição</p>
                            </div>
                            <div class="unidade-local">
                                <p>Matsuda Ji-Paraná - RO</p>
                            </div>
                            <div class="unidade-phone">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p><span>+55</span> (00) 0000-0000</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row footer-site text-center">
                <p><a href="">www.matsuda.com.br</a></p>
            </div>
        </div>
    </div>







</body>
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/ResponsiveSlides/responsiveslides.min.js"></script>
    <link href="js/ResponsiveSlides/responsiveslides.css" rel="stylesheet" type="text/css">

    <!-- bxSlider Javascript file -->
    <script src="js/bxslider/jquery.bxslider.min.js"></script>
    <!-- bxSlider CSS file -->
    <link href="js/bxslider/jquery.bxslider.css" rel="stylesheet" />

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/animation.js"></script>
    <script src="js/menulateral.js"></script>
    <script type="text/javascript" src="js/nora.1.0.min.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
  </body>
</html>
