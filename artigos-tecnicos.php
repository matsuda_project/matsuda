<?php include('topo-pages.php'); ?>
	<div class="col-lg-12 col-xs-12 artigos-tecnicos">
		<div class="container center">
			<div class="artigos-tecnicos--block">
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>PERÍODO SECO: PREVENIR OU REMEDIAR</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="artigos-tecnicos--block">
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>ALIMENTAÇÃO DE EQUÍDEOS COM VOLUMOSO</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>BOVINO QUE BEBE ÁGUA SALOBRA TAMBÉM PRECISA DE SAL MINERAL</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>ALIMENTAÇÃO DE EQUÍDEOS COM VOLUMOSO</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>BOVINO QUE BEBE ÁGUA SALOBRA TAMBÉM PRECISA DE SAL MINERAL</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>ALIMENTAÇÃO DE EQUÍDEOS COM VOLUMOSO</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="row artigos-topico col-lg-7 col-xs-12">
					<a href="">
						<div class="topico-icon left col-lg-1 col-xs-1">
							<div class="text-center">
								<i class="fa fa-angle-right"></i>
							</div>
						</div>
						<div class="topico-content left col-lg-10 col-xs-9">
							<div class="topico-content--title text-center">
								<h1>BOVINO QUE BEBE ÁGUA SALOBRA TAMBÉM PRECISA DE SAL MINERAL</h1>
							</div>
							<div class="topico-content--category text-center">
								<p>Nutrição animal</p>
							</div>
						</div>
						<div class="topico-icon--down left col-lg-1 col-xs-2">
							<div class="text-center">
								<i class="fa fa-download"></i>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>





<?php include('rodapehome.php'); ?>