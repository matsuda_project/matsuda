<div class="col-lg-4 center">
	<div class="row pdm-escolha--container oswald text-center">
		<div class="fazenda-selected">
			<div class="container center">
				<div class="fazenda-selected--name">
					<h1>Fazenda 3 Ilhas</h1>
				</div>
				<div class="fazenda-selected--local">
					<p>Presidente Bernardes - SP</p>
				</div>
			</div>
		</div>
		<div class="fazenda-selected--lote">
			<p>Lote A</p>
		</div>
	</div>
</div>
<div class="col-lg-7 center oswald text-center pdm-escolha--info">
	<div class="row pdm-escolha--text">
		<p>Gado de Corte, a pasto, com 50 animais, início aos 7 meses com média de peso/animal de 105 Kg</p>
	</div>
	<div class="row pdm-escolha--begin">
		<p>Início no programa em 19/08/2016</p>
	</div>
</div>