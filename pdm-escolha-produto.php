<?php include('topo-login.php'); ?>
	<div class="row col-lg-12 col-md-12 col-xs-12 pdm-inicial">
		<div class="container center">
			<div class="row pdm-inicial--container">
				<div class="row pdm-programa--container text-center">
					<div class="lotes lotes-cadastrados">
						<?php include('pdm-section-escolha.php'); ?>
					</div>
				</div>
				<?php include('fazenda-selecionada.php'); ?>
				<div class="row col-lg-12 col-xs-12 produto-escolhido">
					<div class="col-lg-6 center">
						<div class="col-lg-6 container left">
							<div class="row produto-atual text-center oswald">
								<div class="row produto-atual--title">
									<h1>Produto Atual</h1>
								</div>
								<div class="row produto-atual--img">
									<img src="./images/produtos/produto2.png">
								</div>
								<div class="row produto-atual--text ">
									<div class="col-lg-8 center">
										<p>Suplemento Mineral Proteico, Energetico, pronto para uso, em sistema de creep-feeding</p>		
									</div>
								</div>
								<div class="row produto-atual--link">
									<a href="">Mais informações</a>
								</div>
							</div>	
						</div>
						<div class="col-lg-6 container left">
							<div class="row novo-produto text-center oswald">
								<div class="row novo-produto--title">
									<h1>Escolher Outro Produto</h1>
								</div>
								<div class="col-lg-9 center">
									<div class="row novo-produto--text">
										<p>Quer trocar de produto? <span>Digite o nome do Novo Produto</span></p>
									</div>
									<div class="row novo-produto--input">
										<input type="text" name="">
									</div>
								</div>
								<div class="novo-produto--link">
									<a href="" id="btnTrocarProduto">Trocar</a>
								</div>
							</div>
						</div>
					</div>
					<?php include('pdm-grafico.php'); ?>
				</div>
			</div>
		</div>
	</div>









<?php include('modalTrocaProd.php'); ?>







<?php include('rodape-login.php'); ?>