<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Matsuda Portal</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/nora1.0.min.css">3

        <link href="css/bootstrap.icon-large.min.css" rel="stylesheet">
 
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="css/_main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body class="pagesInternas">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="menutopo">
      <div class="container container-fluid">
        <div class="row">
          <div class="col-lg-6 col-xs-12 col-sm-12 col-md-9 segmentos">
            <ul>
              <li><a href="#" class="colorSementes">SEMENTES</a></li>
              <li><a href="#" class="colorNutricaoAnimal">NUTRIÇÃO ANIMAL</a></li>
              <li><a href="#" class="colorInoculantes">INOCULANTES</a></li>
              <li><a href="#" class="colorEquipamentos">EQUIPAMENTOS</a></li>
              <li><a href="#" class="colorEnergiaSolar">ENERGIA SOLAR</a></li>
              <li><a href="#" class="colorPeixes">PEIXES</a></li>
              <li><a href="#" class="colorPet">PET</a></li>
              <li><a href="#" class="colorSaudeAnimal">SAÚDE ANIMAL</a></li>
            </ul>
          </div>
          <div class="col-lg-6 col-xs-12 col-sm-12 col-md-3 redessociais">
            <ul>
              <li><a href="#"><img src="img/iconFace.jpg" alt="Facebook"></a></li>
              <li><a href="#"><img src="img/iconYoutube.png" alt="Youtube"></a></li>
              <li><a href="#"><img src="img/iconInstagran.jpg" alt="Instagran"></a></li>
              <li><a href="#"><img src="img/iconLinkedin.jpg" alt="Linkedin"></a></li>
              <li><a href="#"><img src="img/iconTwitter.jpg" alt="Twitter"></a></li>
              <li><a href="#"><img src="img/iconGoogle.jpg" alt="Google"></a></li>
              <li><a href="#"><img src="img/iconPin.jpg" alt="Pin"></a></li>
            </ul>

          </div>
        </div>
      </div>
    </div>

    <div class="linhaAreaClienteTopo">
      <div class="container container-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-5 linksIntranet">
            <ul>
              <li><a href="#"><img src="img/iconMail.png"> Email</a></li>
              <li><a href="#"><img src="img/iconIntranet.png"> Intranet</a></li>
              <li>
                <div class="custom_select">
                    <div class="custom_select_selected text-center"><img src="./images/icons/flag-brasil.png">
                    </div>
                    <div class="custom_select_icon right">
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <ul class="custom_select_options">
                        <li><a href="#" class="text-center"><img src="./images/icons/flag-brasil.png"></a></li>
                        <li><a href="#" class="text-center"><img src="./images/icons/flag-spain.png"></a></li>
                        <li><a href="#" class="text-center"><img src="./images/icons/flag-usa.png"></a></li>
                    </ul>
                </div>
              </li>
            </ul>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-7 formLoginIntranet">
            <form class="navbar-form navbar-right" role="form">
              <div class="form-group">
                <label>Área do Cliente</label>
              </div>
              <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
              </div>
              <div class="form-group">
                <input type="password" placeholder="Senha" class="form-control">
              </div>
              <button type="submit" class="btn btnMatsuda">OK</button>
            </form>
          </div>
        </div>
      </div>
    </div>   

    <nav class="navbar navbar-default" role="navigation">
      <div class="container container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" id="mostraMenu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="img/logo-interna.png" /> </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="">
          <ul class="nav navbar-nav navbar-right">
            <li class="visible-xs">
              <a href="#" class="linkvideo linkNossosTelefoes">
                <span class="linkmenu">NOSSOS TELEFONES</span>
                <span class="caret"><img src="img/iconTelMenu.png" alt=""></span>
              </a>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="linkmenu">A MATSUDA</span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">Sementes</a></li>
                <li><a href="#">Nutrição Animal</a></li>                
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="linkmenu">PRODUTOS</span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">Sementes</a></li>
                <li><a href="#">Nutrição Animal</a></li>
                <li><a href="#">Inoculantes</a></li>                
                <li><a href="#">Equipamentos</a></li>
                <li><a href="#">Energia Solar</a></li>
                <li><a href="#">Peixes</a></li>
                <li><a href="#">Pet</a></li>
                <li><a href="#">Saúde Animal</a></li>
              </ul>
            </li>
            <li>
              <a href="#" class="linkvideo">
                <span class="linkmenu">VIDEOS</span>
                <span class="caret"></span>
              </a>
            </li>


            <li class="ondecomprar"><a href="#">Onde Comprar?</a></li>
          </ul>         

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container container-fluid-fluid -->
    </nav>

<header id="header">
<div class="header_row collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   <div id="fechaFlutuante" class="clearfix visible-xs">
      <span>X</span>
   </div>


   <div id="tmsearch" class="clearfix">
      <form id="tmsearchbox" method="get" action="">
         <input type="hidden" name="controller" value="search">
         <input type="hidden" name="orderby" value="position">
         <input type="hidden" name="orderway" value="desc">
         <input class="tm_search_query form-control ac_input" type="text" id="tm_search_query" name="search_query" placeholder="Buscar" value="" autocomplete="off">
         <button type="submit" name="tm_submit_search" class="btn btn-default button-search">
         <span>Busca</span>
         </button>
         <span class="btn_show"></span>
      </form>
   </div>

   <div id="layer_cart">
      <div class="clearfix">
         <div class="layer_cart_product col-xs-12 col-md-6">
            <span class="cross" title="Close window"></span>
            <h2>
               <i class="fa fa-ok"></i>
               Produtos adicionados ao carrinho com sucesso.
            </h2>
            <div class="product-image-container layer_cart_img"></div>
            <div class="layer_cart_product_info">
               <span id="layer_cart_product_title" class="product-name"></span>
               <span id="layer_cart_product_attributes"></span>
               <div>
                  <strong class="dark">Qtd</strong>
                  <span id="layer_cart_product_quantity"></span>
               </div>
               <div>
                  <strong class="dark">Total</strong>
                  <span id="layer_cart_product_price"></span>
               </div>
            </div>
         </div>
      </div>
      <div class="crossseling"></div>
   </div>
   <div class="layer_cart_overlay"></div>

   <div id="header-login">

      <div class="header_user_info"><a href="#" onclick="return false;">Sign in</a></div>
      <ul id="header-login-content" class="">
         <li><h3>ÁREA DO CLIENTE</h3></li>
         <li><h2>Quero me Cadastrar</h2></li>
         <li>
            <form action="" method="post" id="header_login_form">
               <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
               <div class="form_content clearfix">
                  <div class="form-group">
                     <input class="is_required validate account_input form-control" data-validate="isEmail" placeholder="E-mail" type="text" id="header-email" name="header-email" value="">
                  </div>
                  <div class="form-group">
                     <span><input class="is_required validate account_input form-control" type="password" placeholder="Password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="" autocomplete="off"></span>
                  </div>          
               </div>
            </form>
         </li>
         <li>
            <p class="submit">
               <button type="button" id="HeaderSubmitLogin" name="HeaderSubmitLogin" class="btn btn-default btn-sm">
               <i class="fa fa-lock left"></i>
               CADASTRAR
               </button>
            </p>           
         </li>

         <li><h2 class="cadastrado">Já Sou Cadastrado</h2></li>
         <li>
            <form action="" method="post" id="header_login_form">
               <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
               <div class="form_content clearfix">
                  <div class="form-group">
                     <input class="is_required validate account_input form-control" data-validate="isEmail" placeholder="E-mail" type="text" id="header-email" name="header-email" value="">
                  </div>
                  <div class="form-group">
                     <span><input class="is_required validate account_input form-control" type="password" placeholder="Password" data-validate="isPasswd" id="header-passwd" name="header-passwd" value="" autocomplete="off"></span>
                  </div>
                  
               </div>
            </form>
         </li>

         <li>
            <p class="create_p p_entrar">
               <a href="#" class="create">ENTRAR</a>
            </p>
          
         </li>

         <li>
            <a href="#" class="esquecisenha">Esqueci minha senha</a>
          
         </li>

          <li>
            <div class="clearfix">
               <a class="btn btn-default btn-sm btn-login-facebook" href="#" title="Login with Your Facebook Account">
               Facebook Login
               </a>              
            </div>
         </li>

      </ul>
   </div>

  <div class="cart_cont clearfix">
      <div class="shopping_cart">
         <a href="#" title="View my shopping cart" rel="nofollow">
         <b>Carrinho</b>
         <span class="ajax_cart_quantity unvisible">2</span>
         <span class="ajax_cart_product_txt unvisible">Produto</span>
         <span class="ajax_cart_product_txt_s unvisible">Produtos</span>
         <span class="ajax_cart_total unvisible">
         </span>
         <span class="ajax_cart_no_product">(empty)</span>
         </a>
         <span class="cart_btn"></span>
         <div class="cart_block block">
            <div class="block_content">
               <div class="cart_block_list">
                  <p class="cart_block_no_products">
                     Nenhum produto
                  </p>
                  <ul class="listaCarrinho">
                    <li>
                      <img src="img/produtoCarrinho.png" alt="Produto">
                      <span>Nome Produto</span><br>
                      <span class="price">R$ 99,90</span>

                    </li>
                    <li>
                      <img src="img/produtoCarrinho.png" alt="Produto">
                      <span>Nome Produto</span><br>
                      <span class="price">R$ 99,90</span>
                    </li>
                  </ul>
                  <div class="cart-prices">
                     
                     <div class="cart-prices-line last-line">
                        <span class="total">Total: </span>
                        <span class="price cart_block_total ajax_block_cart_total">R$ 0.00</span>
                     </div>
                  </div>
                  <p class="cart-buttons">
                     <a id="button_order_cart" class="btn btn-default btn-sm icon-right" href="#" title="Check out" rel="nofollow">
                     <span>
                     Checkout
                     </span>
                     </a>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   
</div>
</header>


  <div class="site">