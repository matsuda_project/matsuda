<?php include('topo-pages.php'); ?>

	<?php include('topo-desempenho.php'); ?>

	<div class="row col-lg-12 col-xs-12 desempenho-grafico" id="block3">
		<div class="container center">
			<div class="desempenho-grafico--container">
				<div class="grafico-block--text left col-lg-3">
					<div class="grafico-block--border left">
					</div>
					<div class="col-lg-6 container">
						<div class="col-lg-12 left">
							<p>PASSE O MOUSE
							SOBRE O PRODUTO
							PARA CONHECER
							SUA INDICAÇÃO</p>
						</div>
					</div>
					<div class="col-lg-4">
						<img src="./images/seta-grafico.png">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="grafico-block--img">
						<img src="./images/grafico.png">
					</div>
				</div>
				<div class="col-lg-3 grafico-block--container">
					<div class="row block-title">
						<div class="col-lg-6 center text-center ">
							<h1>Matsuda Top Bezerro</h1>
						</div>
					</div>
					<div class="row block-content">
						<div class="block-content--img col-lg-5">
							<img src="./images/produto-grafico.png">
						</div>
						<div class="block-content--text col-lg-7">
							<p>Suplemento Mineral proteico energético, pronto para uso, para bezerros em amamentação em sistema creep-feeding</p>
						</div>
					</div>
					<div class="row block-link">
						<div class="text-center">
							<a href="">Mais Informações</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>


<?php include('rodapehome.php'); ?>