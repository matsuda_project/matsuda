<div class="row col-lg-12 col-xs-12 menu-sub">
	<div class="container center">
		<div class="menu-sub--mobile mobile-show tablet-show">
			<div class="block-links text-center">
				<ul>
					<li><a href="">Creep Feeding</a></li>
					<li><a href="">Cria</a></li>
					<li><a href="">Recria</a></li>
					<li><a href="">Engorda</a></li>
					<li><a href="">Confinamento</a></li>
					<li><a href="">Proteico Energético para semi confinamento</a></li>
					<li><a href="">Mistura com sal branco</a></li>
					<li><a href="">Linha Vitta</a></li>
				</ul>
			</div>
		</div>
		<div class="row menu-sub--container mobile-hide tablet-hide">
			<a href="">
				<div class="col-lg-2 col-xs-6 block text-center">
					<div class="col-lg-5 center">
						<span>Creep Feeding</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-5 center align">
						<span>Cria</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-5 center align">
						<span>Recria</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-5 center align">
						<span>Engorda</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-10 center align">
						<span>Confinamento</span>	
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-10 center">
						<span>Proteico Energético para semi confinamento</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center">
					<div class="col-lg-5 center">
						<span>Mistura com sal branco</span>
					</div>
				</div>
			</a>
			<a href="">
				<div class="col-lg-2 block text-center container">
					<div class="col-lg-5 center">
						<span>Linha Vitta</span>
					</div>
				</div>
			</a>
		</div>
		<div class="row col-lg-12 col-xs-12  menu-sub--row center text-center">
			<div class="col-lg-4 col-xs-9 center">
				<div class="menu-sub--block left">
					<a href="">Todas as Categorias</a>
				</div>
				<div class="menu-sub--block left">
					<a href="">Bovinos de Corte</a>
				</div>
				<div class="menu-sub--block left">
					<a href=""><i></i>Retornar</a>
				</div>
			</div>
		</div>
	</div>
</div>
