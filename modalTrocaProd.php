<div class="modalTrocaProduto" id="ModalTroca">
	<div class="troca-block col-lg-12">
		<div class="troca-block--title text-center">
			<p>Ganho de Peso com o Produto Top Bezerro</p>
		</div>
		<div class="col-lg-11 center">
			<div class="row troca-container">
				<div class="troca-container--form oswald text-center">
					<form>
						<div class="row col-lg-12 col-xs-12 col-md-12 control">
							<div class="left col-lg-4 col-xs-4 text-center">
								<label>Data da troca</label>
							</div>
							<div class="left col-lg-6 col-xs-6">
								<input type="text" name="">
							</div>
							<div class="left col-lg-2 col-xs-2 text-center">
								<label><i>DD/MM/AAAA</i></label>
							</div>
						</div>
						<div class="row col-lg-12 col-xs-12 col-md-12 control">
							<div class="left col-lg-4 col-xs-4 text-center">
								<label>Peso Médio do Lote na data da troca</label>
							</div>
							<div class="left col-lg-6 col-xs-6">
								<input type="text" name="">
							</div>
							<div class="left col-lg-2 col-xs-2 text-center">
								<label><i>em Kg</i></label>
							</div>
						</div>
						<div class="row col-lg-12 col-xs-12 col-md-12 control">
							<div class="left col-lg-4 col-xs-4 text-center">
								<label>Motivo da Troca</label>
							</div>
							<div class="left col-lg-6 col-xs-6">
								<textarea></textarea>
							</div>
						</div>
						<button type="submit">OK</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>