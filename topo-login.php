<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Matsuda Portal</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/nora1.0.min.css">
        
        <link href="css/bootstrap.icon-large.min.css" rel="stylesheet">
 
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/_main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
<body>


	<div class="row col-lg-12 col-xs-12 col-md-12 pdm-login">
		<div class="container center">
			<div class="pdm-login--container text-center">
				<div class="pdm-login--user">
					<h1>Olá <span>Aldinei</span></h1>
				</div>
				<div class="pdm-login--content">
					<h2>Você está logado em sua área do cliente</h2>
				</div>
			</div>
		</div>
	</div>