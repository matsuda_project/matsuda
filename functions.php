<?php

	function getResume($content){
		if(strlen($content) > 40){
			$content  = strip_tags($content);
            $content  = trim($content);
			$content = substr($content, 0, 100);
			$pos = strrpos($content, " ");
			$resume = substr($content, 0, $pos)."...";
		}else{
			$resume = $content;
		}
		return $resume;
	}

<?