<div class="row col-lg-12 col-xs-12 menu-sub">
	<div class="container center">
		<div class="menu-sub--mobile mobile-show tablet-show">
			<div class="block-links text-center">
				<ul>
					<li><a href="">Linha Branca</a></li>
					<li><a href="">Proteínado das Águas</a></li>
					<li><a href="">Proteinado de Seca</a></li>
					<li><a href="">Ureado</a></li>
					<li><a href="">Água Dura e Salobra</a></li>
				</ul>
			</div>
		</div>
		<div class="row menu-sub--container sub mobile-hide tablet-hide">
			<div class="col-lg-6 center">
				<a href="">
					<div class="block text-center left">
						<div class="col-lg-5 center">
							<span>Linha Branca</span>
						</div>
					</div>
				</a>
				<a href="">
					<div class="block text-center left">
						<div class="col-lg-7 center">
							<span>Proteínado das Águas</span>
						</div>
					</div>
				</a>
				<a href="">
					<div class="block text-center left">
						<div class="col-lg-7 center">
							<span>Proteinado de Seca</span>
						</div>
					</div>
				</a>
				<a href="">
					<div class="block text-center left">
						<div class="col-lg-5 center align">
							<span>Ureado</span>
						</div>
					</div>
				</a>
				<a href="">
					<div class="block text-center left">
						<div class="col-lg-10 center">
							<span>Água Dura e Salobra</span>	
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="row col-lg-12 col-xs-12  menu-sub--row center text-center">
			<div class="col-lg-3 col-xs-9 center">
				<div class="row">
					<div class="menu-sub--block left">
						<a href="">Bovinos de Corte</a>
					</div>
					<div class="menu-sub--block left">
						<a href="">Cria</a>
					</div>
					<div class="menu-sub--block left">
						<a href=""><i></i>Retornar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>