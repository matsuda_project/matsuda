<div class="row col-lg-12 col-xs-12 desempenho-header">
    <div class="container center">
    	<div class="row">
    		<div class="col-lg-7 center"> 
		        <div class="desempenho-header--container text-center">
		            <div class="container-block left col-lg-4">
		                <img src="./images/desempenhomaximo2.png" alt="">
		            </div>
		            <div class="container-block--text left col-lg-4">
		            	<p>
							<span>Programa</span>
							<span>DESEMPENHO</span>
							<span>MÁXIMO</span>
						</p>
		            </div>
		            <div class="container-block--img left col-lg-4">
		            	<img src="./images/confinamento.png">
		            </div>
		            <div class="row col-lg-12 col-xs-12">
		            	<div class="container-block--icon">
		            		<a href=""><img src="./img/setaBaixo.png"></a>
		            	</div>
		            </div>
		        </div>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="row desempenho-list">
    		<div class="col-lg-12">
    			<ul id="myTabs" role="tablist">
    				<li role="presentation" class="active">
		    			<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 left">
	    					<a href="#entenda" aria-controls="entenda" role="tab" data-toggle="tab">
		    					<div class="desempenho-list--block text-center">
			    					<div class="block-item oswald">
			    						<p>ENTENDA O PROGRAMA DESEMPENHO MÁXIMO / MAIS CARNE CONFINAMENTO</p>
			    					</div>
			    					<div class="block-item--icon">
			    						<i class="fa fa-angle-up"></i>
			    					</div>
		    					</div>
							</a>
		    			</div>
	    			</li>
	    			<li role="presentation">
		    			<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 left">
	    					<a href="#grafico" aria-controls="grafico" role="tab" data-toggle="tab" >
		    					<div class="desempenho-list--block text-center">
			    					<div class="block-item oswald">
			    						<p>GRÁFICO DO TEMPO COM PRODUTOS UTILIZADOS</p>
			    					</div>
			    					<div class="block-item--icon">
			    						<i class="fa fa-angle-up"></i>
			    					</div>
		    					</div>
							</a>
		    			</div>
	    			</li>
	    			<li role="presentation">
		    			<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 left">
	    					<a href="#produtos" aria-controls="produtos" role="tab" data-toggle="tab" >
		    					<div class="desempenho-list--block text-center">
			    					<div class="block-item oswald">
			    						<p>CONHEÇA OS PRODUTOS</p>
			    					</div>
			    					<div class="block-item--icon">
			    						<i class="fa fa-angle-up"></i>
			    					</div>
		    					</div>
							</a>
		    			</div>
	    			</li>
    			</ul>
    		</div>
    	</div>
    </div>
</div>
